<?php 
// root directory
define( 'ABSPATH', __DIR__ . '/' );

// @todo: change this while deploying to production
define( 'HOME_URL', 'http://localhost/speedycfs/' );
define( 'ASSETS_URL', 'http://localhost/speedycfs/public/assets/' );

// sub directory, relative to root folder of the domain, in which the admin application resides.
define( 'SUBDIRECTORY', 'speedycfs' );

// time zone, in which you want to display date and time by default
define( 'TIME_ZONE', 'Asia/Kolkata' );

define( 'ADMIN_API_BASE', 'https://speedycfs.local/ssadmin/api' );
define( 'ADMIN_API_TOKEN', 'c4bfa5bcebc8e0a7e610580bd2dfb398' );

// On local, file_get_contents will throw an ssl error. This constant helps with that.
// Do not define this on dev or production as it poses a security risk.
define( 'ADMIN_API_IGNORE_SSL', true );