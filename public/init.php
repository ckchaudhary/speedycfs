<?php 
ini_set('display_errors', 1);

include( dirname( __FILE__ ) . '/config.php' );

// locale, formatting etc
include_once ABSPATH . 'inc/core.php';

// current request etc
include_once ABSPATH . 'inc/templating.php';