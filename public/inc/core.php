<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// set the default date time zone etc
include_once ABSPATH . 'inc/core/locale.php';

// Formatting and sanitization helper functions
include_once ABSPATH . 'inc/core/formatting.php';

// Formatting and sanitization helper functions
include_once ABSPATH . 'inc/core/query.php';