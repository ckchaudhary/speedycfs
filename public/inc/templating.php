<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// Pagination stuff
include_once ABSPATH . 'inc/templating/functions-pagination.php';
// The singleton object that has details of current http request
include_once ABSPATH . 'inc/templating/class-request.php';
// body_class, site_title, header footer etc.
include_once ABSPATH . 'inc/templating/template-tags.php';
// Get contents from admin panel, using api calls
include_once ABSPATH . 'inc/templating/api-calls.php';

function current_request() {
	return \LogiPrism\SS\Request::instance();
}

current_request()->parse();