<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// Set default date time zone
date_default_timezone_set( \TIME_ZONE );

/**
 * @return \datetimezone
 */
function wp_timezone () {
	return new datetimezone( \TIME_ZONE );
}