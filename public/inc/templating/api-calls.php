<?php 
!defined( 'ABSPATH' ) ? exit() : '';

/**
 * Get one or more items.
 * It may also return null, if nothing found.
 * 
 * @param mixed $args
 * 
 * @return array {
 * 		@type	int 	$total_items 		Total number of items found for given parameters.
 * 		$type 	int 	$current_num_items	Number of items in current set
 * 		@type 	int 	$current_offset		Offset for current set( pagination )
 *		@type 	array 	$items 				Details of individual items in current set
 * }
 */
function get_posts ( $args = '' ) {
	$defaults = [
		// Possible values are : 'page', 'blog'
		// You can pass multiple by separating each by comma, e.g: 'page, blog'
		'type' 		=> 'page',

		// No imaginable use of this field yet.
		// 'subtype' 		=> '',

		// Start from this offset
		'offset' 	=> 0,

		// Max number of results to return( starting from given offset )
		'max' 		=> 10,

		// Possbile values are : 'date', 'title'
		'order_by' 	=> 'date',

		// Possible values are : 'DESC', 'ASC'. Capitalization is irrelevant.
		'order' 	=> 'DESC',

		// Get a particular content, with this slug.
		'slug' 		=> false,

		// Only entries whose title match the given search term
		'search'	=> '',

		// Get a particular content, with this id.
		// If you pass this, all other agruments above it become irrelevant.
		'id' 		=> false,

		// If the api returns an error, instead of showing the error text, just return null.
		// Helpful in cases where you don't want to be bothered with error handling.
		'null_on_error' => true,
	];

	$args = wp_parse_args( $args, $defaults );

	// This is crucial for api, do not change this unless you know exactly what you are doing.
	$args[ 'endpoint' ] = 'list-contents';

	return _admin_api_get( $args );
}

/**
 * Get meta value.
 * 
 * @param int 		$content_id 	Id of page, 'blog', gallery etc.
 * @param string 	$content_type 	'page', 'blog', 'gallery' etc.
 * @param string 	$meta_key 		Name of meta key
 * 
 * @return mixed|null null if nothing found.
 */
function get_meta ( $content_id, $meta_key, $content_type = '' ) {
	$args = [
		'endpoint' => 'get-meta',
		'content_id' => absint( $content_id ),
		'content_type' => sanitize_text_field( $content_type ),
		'meta_key' => sanitize_text_field( $meta_key ),
	];

	return _admin_api_get( $args );
}

/**
 * Call the admin api with given parameters and return value.
 * 
 * @param mixed $args {
 * 		@type bool $null_on_error Whether to return null if api call fails or returns with error. Default true.
 * }
 * 
 * @todo: make the $null_on_error parameter work.
 * 
 * @return array|null 
 */
function _admin_api_get ( $args ) {
	$defaults = [
		'null_on_error' => true,
	];

	$args = wp_parse_args( $args, $defaults );

	$null_on_error = $args['null_on_error'];
	unset( $args['null_on_error'] );

	$args[ '_token' ] = ADMIN_API_TOKEN;
	$api_url = add_query_arg( $args, ADMIN_API_BASE );

	if ( defined( 'ADMIN_API_IGNORE_SSL' ) && ADMIN_API_IGNORE_SSL ) {
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);
		$response = file_get_contents( $api_url, false, stream_context_create( $arrContextOptions ) );
	} else {
		$response = file_get_contents( $api_url, false );
	}

	$response = @json_decode( $response, true );
	if ( empty( $response ) || $response[ 'status' ] !== 'success' ) {
		return null;
	}

	return $response[ 'data' ];
}