<?php 
!defined( 'ABSPATH' ) ? exit() : '';

function header_metas () {
	?>
	<meta charset="<?php echo SITE_CHARSET; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="<?php echo HOME_URL ?>favicon.ico">
	<link rel="icon" href="<?php echo HOME_URL ?>favicon.svg" type="image/svg+xml">
	<link rel="apple-touch-icon" href="<?php echo HOME_URL ?>apple-touch-icon.png">
	<?php 
	$title = current_request()->get_details( 'title' );
	echo "<title>". stripslashes( $title ) ."</title>";

	$meta_description = current_request()->get_details( 'meta-description' );
	if ( $meta_description ) {
		echo "<meta name='description' content='". esc_attr( $meta_description ) ."'>";
	}

	$meta_keywords = current_request()->get_details( 'meta-keywords' );
	if ( $meta_keywords ) {
		echo "<meta name='keywords' content='". esc_attr( $meta_keywords) ."' >";
	}

	// @site-specific
	echo "<meta name='author' content='SpeedyCFS' >";

	// @todo: add og: meta
}

function header_assets () {
	echo '<link rel="stylesheet" href="'. ASSETS_URL . 'css/style.css">';
	echo '<link rel="stylesheet" href="'. ASSETS_URL . 'css/slick.css">';
}

function footer_assets () {
	// First, jquery
	echo '<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>';

	// Slick slider
	echo '<script src="'. ASSETS_URL .'js/slick.min.js"></script>';

	// Custom script
    echo '<script src="'. ASSETS_URL .'js/custom.js"></script>';
}

function body_class () {
	$body_classes = current_request()->get_details( 'body_classes' );
	if ( $body_classes ) {
		echo implode( ' ', $body_classes );
	}
}

function get_template_part( $slug, $name = null, $args = array() ) {
    $templates = array();
    $name      = (string) $name;
    if ( '' !== $name ) {
        $templates[] = "{$slug}-{$name}.php";
    }
 
    $templates[] = "{$slug}.php";

    if ( ! locate_template( $templates, true, false, $args ) ) {
        return false;
    }

	return true;
}

function locate_template( $template_names, $load = false, $require_once = true, $args = array() ) {
    $located = '';
    foreach ( (array) $template_names as $template_name ) {
        if ( ! $template_name ) {
            continue;
        }

		if ( file_exists( ABSPATH . 'templates/' . $template_name ) ) {
            $located = ABSPATH . 'templates/' . $template_name;
            break;
        }
    }
 
    if ( $load && '' !== $located ) {
        if ( $require_once ) {
			require_once $located;
		} else {
			require $located;
		}
    }
 
    return $located;
}