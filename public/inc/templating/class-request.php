<?php 
namespace LogiPrism\SS;

class Request {
	#region bootstrap

	// Information parsed from url
	protected $_request = [];

	// Information supplied by templates
	protected $_current = [];

	private function __construct() {
	}

	public static function instance() {
		static $instance;
		if ( ! isset( $instance ) ) {
			$instance = new self();
		}
		return $instance;
	}

	#endregion

	#region Current request details
	public function parse () {
		$parts = parse_url( $_SERVER['REQUEST_URI'] );
		$path = isset( $parts['path'] ) ? trim( $parts['path'], '/' ) : '';
		if ( $path ) {
			// If the admin resides in a subdirectory, dont consider that subdirectory for page name
			if ( SUBDIRECTORY ) {
				$sub_directory = trim( SUBDIRECTORY, '/' );
				if ( strpos( $path, $sub_directory ) === 0 ) {
					$path = substr( $path, strlen( $sub_directory ) );
					$path = trim( $path, '/' );
				}
			}
		}

		if ( $path ) {
			$this->_request[ 'pagenames' ] = explode( '/', $path );
		}

		$this->_request[ 'params' ] = [];
		$qs = isset( $parts['query'] ) ? $parts['query'] : '';
		if ( $qs ) {
			$qs = explode( '&', $qs );
			foreach ( $qs as $k => $v ) {
				$this->_request[ 'params' ][ $k ] = $v;
			}
		}
	}

	public function get_path_parts () {
		return isset( $this->_request['pagenames'] ) ? $this->_request[ 'pagenames' ] : [];
	}

	public function template_to_load () {
		$pagenames = $this->get_path_parts();
		if ( empty( $pagenames ) ) {
			return ABSPATH . 'templates/home.php';
		}

		$stack = [];
		foreach ( $pagenames as $pagename ) {
			$stack[] = $pagename;
		}

		$found = false;
		while ( !$found ) {
			$template = ABSPATH . 'templates/' . implode( '/', $stack ) . '.php';
			if ( file_exists( $template ) ) {
				$found = $template;
			} else {
				array_pop( $stack );
				if ( count( $stack ) == 0 ) {
					break;
				}
			}
		}

		if ( !$found ) {
			$found = ABSPATH . 'templates/404.php';
		}

		return $found;
	}

	#endregion

	#region Current page details

	public function get_details( $key = '' ) {
		if ( $key ) {
			if ( isset( $this->_current[$key] ) ) {
				return $this->_current[$key];
			} else {
				return null;
			}
		}
		
		return $this->_current;
	}

	public function set_details( $details ) {
		if ( empty( $details ) ) {
			return false;
		}

		foreach ( $details as $key => $val ) {
			// @todo: perform checks for special info
			$this->_current[$key] = $val;
		}
	}

	#endregion
}