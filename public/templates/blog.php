<?php 
/**
 * Template for about us page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

$uri_path = current_request()->get_path_parts();
$template_to_load = 'archive';
if ( isset( $uri_path[ 1 ] ) && 'page' != $uri_path[ 1 ] ) {
	$template_to_load = 'single';
}

get_template_part( 'blog-' . $template_to_load );