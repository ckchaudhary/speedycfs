<?php 
/**
 * Template for about us page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 			=> 'Blog - SpeedyCFS',
	'meta-description'  => "some details about the site",
	'meta-keywords'  	=> "cargo, logitstics",
	'body_classes' 		=> [ 'blog' ],
] );

include ABSPATH . 'templates/parts/header.php';
?>
<h1>Blog</h1>

<?php 
$uri_path = current_request()->get_path_parts();
$current_page = isset( $uri_path[ 1 ] ) && 'page' == $uri_path[ 1 ] && isset( $uri_path[ 2 ] ) ? absint( $uri_path[ 2 ] ) : 1;
$current_page = $current_page > 0 ? $current_page : 1;

$per_page = 3;
$offset = ( $current_page -  1 ) * $per_page;

$posts = get_posts([
	'type' 		=> 'blog',
	'offset' 	=> $offset,
	'max' 		=> $per_page,
]);

if ( $posts[ 'total_items' ] > 0 ){
	echo '<div class="blog-list-wrapper list-items-wrapper">';
}

if ( $posts[ 'num_items' ] > 0 ) {
	// There are blog posts to display

	echo "<div class='list-items'>";

	foreach ( $posts[ 'items' ] as $entry ) {
		get_template_part( 'parts/blog', 'archive', [ 'entry' => $entry ] );
	}

	echo "</div><!-- /.list-items -->";
} else {
	// Nothing to display
	?>
	<p class="notice notice-warning no-items">No entries found.</p>
	<?php 
}

if ( $posts[ 'total_items' ] > 0 && $posts[ 'total_items' ] > $per_page ) {
	// Total number of posts are more than batch size. We need pagination links
	get_template_part( 'parts/pagination', null, [ 
		'total' 		=> $posts[ 'total_items' ], 
		'per_page' 		=> $per_page, 
		'current_page' 	=> $current_page,
	] );
}

if ( $posts[ 'total_items' ] > 0 ) {
	echo "</div><!-- /.blog-list-wrapper -->";
}
?>

<?php 
include ABSPATH . 'templates/parts/footer.php';