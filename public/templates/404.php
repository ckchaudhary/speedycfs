<?php 
/**
 * Template for 404 page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' => '404 Not found',
	'meta-description'  => "some details about the site",
	'meta-keywords'  => "cargo, logitstics",
	'body_classes' => [ 'error', '404' ],
] );

include ABSPATH . 'templates/parts/header.php';
?>
<h3>404 Not Found</h3>
<?php 
include ABSPATH . 'templates/parts/footer.php';