<?php 
/**
 * Template for about us page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 			=> 'About Us',
	//'meta-description'  => "some details about the site",
	//'meta-keywords'  	=> "cargo, logitstics",
	'body_classes' 		=> [ 'about', 'has-children' ],
] );

get_template_part( 'parts/header' );
?>
<h3>About Us</h3>

<?php 
$output = get_meta( 2, 'gallery_items' );
var_dump($output);
?>

<?php 
get_template_part( 'parts/footer' );