<?php 
/**
 * Template for about us page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 			=> 'About Us',
	'meta-description'  => "some details about the site",
	'meta-keywords'  	=> "cargo, logitstics",
	'body_classes' 		=> [ 'about', 'has-children' ],
] );

include ABSPATH . 'templates/parts/header.php';
?>
<h3>About > Services</h3>
<?php 
include ABSPATH . 'templates/parts/footer.php';