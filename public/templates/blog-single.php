<?php 
/**
 * Template for about us page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

$uri_path = current_request()->get_path_parts();
$blog_slug = isset( $uri_path[ 1 ] ) ? $uri_path[ 1 ] : '';

$posts = get_posts( [
	'type' 	=> 'blog',
	'slug'	=> $blog_slug,
] );

if ( !$posts || $posts[ 'total_items' ] == 0 ) {
	get_template_part( '404' );
	return false;
}

$entry = $posts[ 'items' ][ 0 ];

current_request()->set_details( [
	'title' 			=>  $entry['title'] . '- SpeedyCFS',
	'meta-description'  => "some details about the site",
	'meta-keywords'  	=> "cargo, logitstics",
	'body_classes' 		=> [ 'blog' ],
] );

include ABSPATH . 'templates/parts/header.php';

get_template_part( 'parts/blog', 'single', [ 'entry' => $entry ] );

include ABSPATH . 'templates/parts/footer.php';