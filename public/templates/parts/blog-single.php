<?php 
$entry = $args['entry'];
?>
<article class='blog-item list-item type-<?php echo $entry[ 'type' ];?>' id='item-<?php echo $entry[ 'id' ];?>'>
	<header class='item-header'>
		<h1><?php echo stripslashes( $entry[ 'title'] );?></h1>
	</header>

	<div class='item-details'>
		<p class='item-date'><?php echo date( DATE_FORMAT, $entry[ 'created_at' ] );?></p>
		<p class='item-summary'>
			<?php echo stripslashes( $entry[ 'details' ] );?>
		</p>
	</div>

	<footer>
		
	</footer>
</article>