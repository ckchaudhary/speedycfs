	<footer id="site-footer">
        <div class="container">
            <div class="footer-top"><img src="<?php echo ASSETS_URL;?>images/phone-footer.svg" alt="" /> Need quick help? Talk to us <span>+91 22 67950900</span></div>
            <div class="footer-bottom"><span>© 2022. All rights reserved</span><a href="">Speedy Multimodes Ltd</a><a href="">Legal Disclaimer</a></div>
        </div>
    </footer>

	<?php footer_assets();?>
	</body>
</html>