<!DOCTYPE html>
<html lang="en">
	<head>
		<?php header_metas(); ?>
		<?php header_assets(); ?>
	</head>
	<body class="<?php body_class();?>" >

		<div id="top-header">
			<div class="container">
				<div class="top-header-inner display-flex">
					<div class="site-mob">Talk to us: +91 22 67950900 </div>
					<div class="top-header-menu">
						<ul>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Media</a></li>
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Downloads</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div id="header">
			<div class="container">
				<div class="header-master display-flex">
					<div class="site-branding">
						<a href="#"><img src="<?php echo ASSETS_URL;?>images/logo.svg" alt="site-logo" /></a>
					</div>
					<nav id="main-menu">
						<ul>
							<li><a href="<?php echo HOME_URL;?>">Home</a></li>
							<li><a href="<?php echo HOME_URL;?>about-us">About Us</a></li>
							<li><a href="#">Services</a></li>
							<li><a href="#">Our Facilities</a></li>
							<li><a href="#">Tool</a></li>
						</ul>
						<div class="header-search">
							<form>
								<button class="icon-search">search</button>
							</form>
						</div>
					</nav>
				</div>
			</div>
		</div>