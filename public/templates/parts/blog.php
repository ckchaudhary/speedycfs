<?php 
$entry = $args['entry'];
?>
<article class='blog-item list-item type-<?php echo $entry[ 'type' ];?>' id='item-<?php echo $entry[ 'id' ];?>'>
	<header class='item-header'>
		<h1><a href='<?php echo HOME_URL . 'blog/' . $entry[ 'slug' ];?>'><?php echo stripslashes( $entry[ 'title'] );?></a></h1>
	</header>

	<div class='item-details'>
		<p class='item-date'><?php echo date( DATE_FORMAT, $entry[ 'created_at' ] );?></p>
		<p class='item-summary'>
			<?php 
			$summary = stripslashes( $entry[ 'details' ] );
			$summary = wp_strip_all_tags( $summary, true );
			
			echo truncate_string( $summary, 300, '', true );
			?>
		</p>
	</div>

	<footer>
		<a href='<?php echo HOME_URL . 'blog/' . $entry[ 'slug' ];?>'>Read More</a>
	</footer>
</article>