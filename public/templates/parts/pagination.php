<div class='pagination <?php echo isset( $args['cssclass'] ) ? $args['cssclass'] : '';?>'>
	<div class='description'>
		<?php
		$started_at = ( ( $args[ 'current_page' ] -  1 ) * $args[ 'per_page' ] ) + 1;
		$end_at = $args[ 'current_page' ] * $args[ 'per_page' ] < $args[ 'total' ] ? $args[ 'current_page' ] * $args[ 'per_page' ] : $args[ 'total' ];
		printf( "Showing %s - %s of %s", $started_at, $end_at, $args[ 'total' ] );
		?>
	</div>

	<div class='links'>
		<?php 
		$uri_path = current_request()->get_path_parts();
		$base_url = trailingslashit( HOME_URL . ( !empty( $uri_path ) ? $uri_path[ 0 ] : '' ) );
		emi_generate_paging( $args[ 'total' ], $args[ 'per_page' ], $args[ 'current_page'], $base_url );
		?>
	</div>
</div>