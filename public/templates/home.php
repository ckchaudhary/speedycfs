<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' => 'Home Page',
	//'meta-description'  => "some details about the site",
	//'meta-keywords'  => "cargo, logitstics",
	'body_classes' => [ 'home' ],
] );

get_template_part( 'parts/header' );
?>


	<div id="hero-section">
        <div class="container">
            <div class="hero-inner-section display-flex">
                <div class="hero-section-left">
                    <div class="hero-section-banner-img"><img src="<?php echo ASSETS_URL;?>images/hero-img.jpg" alt="" /></div>
                    <h1><span>Impeccable CFS services</span>closest to the port!</h1>
                </div>

                <div class="hero-section-right">
                    <div class="hero-youtube-frame">youtube</div>
                    <div class="hero-section-icon">
                        <div class="hero-section-icon-blocks">
                            <div class="hero-icon-block"><img src="<?php echo ASSETS_URL;?>images/attach.svg" alt="" /></div>
                            <h4>Other Services</h4>
                        </div>
                        <div class="hero-section-icon-blocks">
                            <div class="hero-icon-block"><img src="<?php echo ASSETS_URL;?>images/proforma.svg" alt="" /></div>
                            <h4>e-Proforma</h4>
                        </div>
                        <div class="hero-section-icon-blocks">
                            <div class="hero-icon-block"><img src="<?php echo ASSETS_URL;?>images/download.svg" alt="" /></div>
                            <h4>Download Tariff</h4>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="content-about">
        <div class="container">
            <p>As you facilitate Exim trade from across the world, rely on us for strategically located state-of-the-art CFS facilities at JNPT (Nhava Sheva) and Mundra. The close vicinity to the port, backed by the best in safety and technology, helps you
                enhance the efﬁciency of your global and domestic business supply chains.</p>
            <p>Get in touch to give your business the advantage of quick, efﬁcient CFS services and make imports and exports your biggest strength.</p>
        </div>
    </div>

    <div class="home-services-section">
        <div class="container">
            <h2>Services that deliver the best to your business</h2>
            <p>Along with comprehensive CFS services that make it convenient and easy to consolidate and deconsolidate your cargo, you also beneﬁt from a host of our other offerings. Whether it is Direct Port Delivery (DPD), bonded and non-bonded warehousing,
                or reefer services, hazardous cargo handling and value-added offerings, we do it all. When you choose us, you can be sure your cargo is in the best possible hands!</p>
            <p>In keeping with our approach to be digitally-enabled and future-ready, our myCFS Speedy portal lets you share information, get real-time updates and complete a number of tasks without requiring physical presence at the CFS.</p>
            <p>Connect with us or register on myCFS Speedy for CFS services backed by unmatched speed and safety.</p>
            <div class="services-slide-block">
                <div class="services-slider">
					<?php 
					$slide_items = get_meta( 2, 'gallery_items' );
					if ( !empty( $slide_items ) ) {
						$slide_items = @json_decode( $slide_items, true );
						foreach ( $slide_items as $slide_item ) {
							?>
							<div class="services-slider-column">
								<img src="<?php echo esc_attr( $slide_item[ 'img_url' ] );?>" alt="<?php echo esc_attr( $slide_item[ 'img_alt' ] );?>" />
								<h3><a href="<?php echo esc_attr( $slide_item[ 'link_url' ] );?>"><?php echo $slide_item[ 'link_text' ];?></a></h3>
							</div>
							<?php 
						}
					}
					?>
                </div>
                <div class="service-more"><a href="#">Know More</a></div>
            </div>
        </div>
    </div>

    <div class="facilitie-testimonal">
        <div class="container">
            <div class="facilitie-testimonal-block">
                <div class="facilitie-left">
                    <div class="icon"><img src="<?php echo ASSETS_URL;?>images/facilities-icon.svg" alt="" /></div>
                    <div class="facilitie-left-content">
                        <h3>Our Facilities</h3>
                        <div class="facilitie-links"><a href="#">JNPT</a><a href="#">Mundra</a></div>
                    </div>
                </div>

                <div class="testimonal-slider-block">
                    <h4>What our customers say</h4>
                    <div class="testimonals-slider">
                        <div>
                            <p>Whenever we need any agency for dispatch of consignment be it night or a holiday, Speedy has shown their commitment, punctuality and dedication.</p>
                            <span>- Siddhartha Rao, Indian Oil</span>
                        </div>
                        <div>
                            <p>Whenever we need any agency for dispatch of consignment be it night or a holiday, Speedy has shown their commitment, punctuality and dedication.</p>
                            <span>- Siddhartha Rao, Indian Oil</span>
                        </div>
                        <div>
                            <p>Whenever we need any agency for dispatch of consignment be it night or a holiday, Speedy has shown their commitment, punctuality and dedication.</p>
                            <span>- Siddhartha Rao, Indian Oil</span>
                        </div>
                        <div>
                            <p>Whenever we need any agency for dispatch of consignment be it night or a holiday, Speedy has shown their commitment, punctuality and dedication.</p>
                            <span>- Siddhartha Rao, Indian Oil</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php 
get_template_part( 'parts/footer' );