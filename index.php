<?php 
/**
 * The main entry point for public facing site
 */

// Load everything!
include 'public/init.php';

$template_to_load = current_request()->template_to_load();
include_once $template_to_load;