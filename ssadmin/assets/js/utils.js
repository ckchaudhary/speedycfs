function copyToClipboard ( text ) {
	const elem = document.createElement('textarea');
	elem.value = text;
	document.body.appendChild(elem);
	elem.select();
	document.execCommand('copy');
	document.body.removeChild(elem);
}

function show_notice( message, args = {} ) {
	args = jQuery.extend( true, {
		'is_dismissable' 	: true,
		'type'				: 'info'
	}, args );

	jQuery( '.wrap' ).before( `<div class='alert alert-${args.type} ${args.is_dismissable ? 'alert-dismissible fade show' : ''}' role='alert'>${message} ${args.is_dismissable ? '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' : ''}</div>` );
};

function show_toast ( message ) {
	let template = `<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" style="position: fixed; top: 20px; right: 20px; z-index: 1040;">
  		<div class="toast-header justify-content-between">
		  	<strong class="mr-auto">Done</strong>
  		</div>
  		<div class="toast-body">
    		${message}
  		</div>
	</div>`;

	jQuery( '.toast' ).remove();
	jQuery( 'body' ).append( template );
	jQuery( '.toast' ).toast( {
		autohide : true,
		delay: 1200,
	} ).toast( 'show' );
}

/**
 * Format bytes as human-readable text.
 * 
 * Tags: format size, human readable size, bytes to mb, bytes to gb
 * 
 * @param bytes Number of bytes.
 * @param si True to use metric (SI) units, aka powers of 1000. False to use 
 *           binary (IEC), aka powers of 1024.
 * @param dp Number of decimal places to display.
 * 
 * @return Formatted string.
 */
function humanFileSize ( bytes, si = false, dp = 1 ) {
	const thresh = si ? 1000 : 1024;
  
	if (Math.abs(bytes) < thresh) {
	  	return bytes + ' B';
	}
  
	const units = si 
	  	? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
	  	: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
	let u = -1;
	const r = 10**dp;
  
	do {
	  	bytes /= thresh;
	  	++u;
	} while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);
  
	return bytes.toFixed(dp) + ' ' + units[u];
}

/**
 * Convert the given amount in bytes/kb/mb/gb into bytes/kb/mb/gb 
 * This function uses binary system. I.e: 1kb = 1024 bytes and not 1000bytes
 * 
 * Tags: convert size, convert bytes, mb to gb, kb to mb, bytes to kb, bytes to mb
 * 
 * @param {Object} args {
 * 		@type {int} amount - Amount to be converted
 * 		@type {string} from - source unit
 * 		@type {string} to - destination unit
 * 		@type {int} precision number of units after decimal, when applicable
 * }
 * 
 * @return {float}
 */
function convertBytes ( args ) {
	let amount = args[ 'amount' ];
	let from = args[ 'from' ];
	let to = args[ 'to' ].toLowerCase();

	// first get the amount in bytes
	let amount_in_bytes = 0;
	switch ( from ) {
		case 'kb':
			amount_in_bytes = amount * 1024;
			break;
		
		case 'mb':
			amount_in_bytes = amount * 1024 * 1024;
			break;

		case 'gb':
			amount_in_bytes = amount * 1024 * 1024 * 1024;
			break;

		default: 
			amount_in_bytes = amount;
			break;
	}

	// Now covert in desired unit
	let amount_converted = 0;
	switch ( to ) {
		case 'kb':
			amount_converted = amount_in_bytes / 1024;
			break;
		
		case 'mb':
			amount_converted = amount_in_bytes / ( 1024 * 1024 );
			break;

		case 'gb':
			amount_converted = amount_in_bytes / ( 1024 * 1024 * 1024 );
			break;

		default: 
			amount_converted = amount_in_bytes;
			break;
	}

	return amount_converted.toFixed( args['precision'] );
}