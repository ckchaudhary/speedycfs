jQuery( ($) => {
	$('.item-list-table').on( 'click', 'a.btn_delete_item', function( e ){
		e.preventDefault();

		let r = confirm( 'Are you sure you want to delete this item? This can not be undone.' );
		if ( !r ) {
			return false;
		}

		let $el = $(this);
		let $tr = $el.closest( 'tr' );
		if ( $tr.data( 'doing-ajax' ) ) {
			return false;
		}

		$tr.data( 'doing-ajax', true );
		$tr.addClass( 'table-danger' );
		$tr.find( 'td.preview' ).html( '<div class="spinner-border spinner-border-sm" role="status"></div>' );

		$.ajax( {
			url: $el.attr( 'href' ),
		} ).done( function( response ) {
			response = jQuery.parseJSON( response );
			if ( response.status === 'success' ) {
				$tr.remove();
				show_notice( response.data.message, { 'type' : 'success' } );
			} else {
				show_notice( response.data.message, { 'type' : 'danger' } );
			}
		});
	});

	$('.item-list-table').on( 'click', '[data-copy_text]', function(e){
		e.preventDefault();
		let text_to_copy = $(this).data( 'text_to_copy' );
		copyToClipboard( text_to_copy );
		show_toast( 'Copied to clipboard' );
	} );
});