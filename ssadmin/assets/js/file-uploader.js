class MediaUploader {
	options = {
		//'extensions_allowed' : [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'zip' ],
		'extensions_allowed' : [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp' ],
		'max_size' : 50,// 50mb
	};

	_l = new Map();

	upload_in_progress = false;

	constructor ( options ) {
		const _this = this;

		// Ovewrite default options		
		_this.options = jQuery.extend( true, {}, _this.options, options );
	};

	init () {
		const _this = this;

		let $form = jQuery('#frm_hidden_uploader');
		if ( !$form.length ) {
			return false;
		}

		_this._l.set( 'form', $form );
		_this._l.set( 'file_input', $form.find( 'input[type="file"]' ) );
		_this._l.set( 'open_uploader', jQuery( '#btn_add_new_media' ) );
		_this._l.set( 'table', jQuery( 'table.item-list-table' ) );

		// set accepted file types
		_this._l.get( 'file_input' ).attr( 'accept', '.' + _this.options.extensions_allowed.join( ',.' ) );

		_this._l.get( 'open_uploader' ).click( function(e){
			e.preventDefault();

			// Abort if another upload is ongoing
			if ( _this.upload_in_progress ) {
				return false;
			}

			_this._l.get('file_input').trigger('click');
		});

		_this.setup_uploader();
	};

	setup_uploader () {
		const _this = this;

		// Submit form when a file is chosen
		_this._l.get( 'file_input' ).change( function(e){
			e.preventDefault();
			if ( e.target.files.length < 1 ) {
				return false;
			}

			if ( _this.upload_in_progress ) {
				show_notice( 'Please wait untill previous upload is completed.', { 'type' : 'danger' } );
				this.value = null;
				return false;
			}

			let is_valid = true;
			for ( const file of e.target.files ) {
				let retval = _this.validate_before_upload( file );
				if ( retval.status == 'error' ) {
					show_notice( retval.message, { 'type' : 'danger' } );
					is_valid = false;
					break;
				}
			}

			if ( !is_valid ) {
				return false;
			}

			// show preview
			const file = e.target.files[ 0 ];
			let preview_row = `<tr class="upload_preview">
				<td class="preview"><div class="spinner-border spinner-border-sm" role="status"></div></td>
				<td class="title">${file.name}</td>
				<td class="date">-</td>
				<td class="action">-</td>
			</tr>`;

			_this._l.get( 'table' ).find( 'tbody' ).prepend( preview_row );
			_this._l.get( 'table' ).find( 'tr.row-no-items' ).remove();

			// Submit form
			_this._l.get('form').submit();
		});

		let args = {
			beforeSubmit: function( arr, $form, options) { 
				_this.upload_in_progress = true;
				_this._l.get( 'open_uploader' ).addClass( 'disabled' );
			},
			success: function ( response ) { 
				response = jQuery.parseJSON( response );
				let $tr = _this._l.get( 'table' ).find( 'tbody tr.upload_preview' );
				if ( response.status === 'success' ) {
					for ( let p in response.data ) {
						let $td = jQuery( 'td.' + p, $tr );
						if ( $td.length ) {
							$td.html( response.data[ p ] );
						}
					}

					$tr.removeClass( 'upload_preview' );
					show_notice( 'Upload successful.', { 'type' : 'success' } );
				} else {
					show_notice( response.data.message, { 'type' : 'danger' } );
					$tr.remove();
				}

				_this._l.get( 'form' ).resetForm();
				_this.upload_in_progress = false;
				_this._l.get( 'open_uploader' ).removeClass( 'disabled' );
			}, 
			error: function () {
				_this._l.get( 'table' ).find( 'tbody tr.upload_preview' ).remove();
				show_notice( 'Upload failed. Please reload the page and try again.', { 'type' : 'danger' } );
				_this._l.get( 'form' ).resetForm();
				_this.upload_in_progress = false;
			},
		};
		_this._l.get( 'form' ).ajaxForm( args );
	};

	validate_before_upload ( file ) {
		let retval = {
			'status' : 'success',
			'message' : '',
		};

		let extension = file.name.split( '.' ).pop().toLowerCase();
		// is extension allowed?
		let extensions_allowed = this.options[ 'extensions_allowed' ];
		if ( extensions_allowed.indexOf( extension ) === -1 ) {
			retval.status = 'error';
			let extensions_allowed_csv = '.' + extensions_allowed.join( ', .' );
			retval.message = `Please only upload ${extensions_allowed_csv} files.`;
			return retval;
		}

		// max size
		let size = file.size;
		let allowed_max_size = convertBytes({ 
			'amount' : this.options['max_size'], 
			'from' : 'mb',
			'to' : 'b',//bytes
		});

		if ( size > allowed_max_size ) {
			retval.status = 'error';
			retval.message = `Please only upload files upto ${this.options['max_size']}MB`;
			return retval;
		}

		return retval;
	};

}

jQuery( ($) => {
	window.media_uploader = new MediaUploader();
	window.media_uploader.init();
});