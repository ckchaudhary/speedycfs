jQuery( ($) => {
	// Initialize Rich Text Editors
	$('.rteditor').each(function(){
		$(this).summernote({
			minHeight: 100,
			/*toolbar: [
				['style', ['style']],
				['font', ['bold', 'underline', 'clear']],
				['fontname', ['fontname']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture', 'video']],
				['view', ['fullscreen', 'codeview', 'help']],
			],*/
		});
	});

	$('#btn_reset_url').click( function( e ){
		e.preventDefault();

		let r = confirm( "System will regenrate url for this item, based on title, when you save next. Are you sure you want to continue?" );
		if ( !r ) {
			return false;
		}

		$( this ).attr( 'disabled', 'disabled' );
		$( 'input[name="content_identifier"]' ).val('');
		$( '.content_url' ).html( '<span class="text-muted">..to be generated..</span>' );
	});

	$('#btn_delete_content').on( 'click', function( e ){
		e.preventDefault();

		let r = confirm( 'Are you sure you want to delete this item? This can not be undone.' );
		if ( !r ) {
			return false;
		}

		let $el = $(this);
		if ( $el.data( 'doing-ajax' ) ) {
			return false;
		}

		$el.data( 'doing-ajax', true );
		$el.addClass( 'disabled' );

		$.ajax( {
			url: $el.attr( 'href' ),
		} ).done( function( response ) {
			response = jQuery.parseJSON( response );
			if ( response.status === 'success' ) {
				show_notice( response.data.message, { 'type' : 'success' } );
				setTimeout( function(){ window.location.href = response.data.list_url; }, 3000 );
			} else {
				show_notice( response.data.message, { 'type' : 'danger' } );
			}
		});
	});

	if ( window.gallery_manager_items ) {
		new GalleryManager( window.gallery_manager_items );
	}
});

class GalleryManager {
	items = [];
	_l = new Map();

	_template = `<div class="card bg-dark text-white mt-3 single-item" data-index="{{{INDEX}}}">
  		<img src="{{{IMG_URL}}}" class="card-img" alt="{{{IMG_ALT}}}" style="height: 280px;">
  		<div class="card-img-overlay">
		  	<div class="input-group mb-3">
			  	<span class="input-group-text">Image Url</span>
			  	<input type="text" class="form-control" data-field="img_url" value="{{{IMG_URL}}}">
		  		<span class="input-group-text">Alt Text</span>
		  		<input type="text" class="form-control" data-field="img_alt" value="{{{IMG_ALT}}}">
			</div>

			<div class="input-group mb-3">
			  	<span class="input-group-text">Title</span>
			  	<input type="text" class="form-control" data-field="title" value="{{{TITLE}}}">
			</div>

			<div class="input-group mb-3">
			  	<span class="input-group-text">Subtitle</span>
			  	<input type="text" class="form-control" data-field="subtitle" value="{{{SUBTITLE}}}">
			</div>

			<div class="input-group mb-3">
			  	<span class="input-group-text">Link Url</span>
			  	<input type="text" class="form-control" data-field="link_url" value="{{{LINK_URL}}}">
		  		<span class="input-group-text">Link Text</span>
		  		<input type="text" class="form-control"  data-field="link_text" value="{{{LINK_TEXT}}}">
			</div>

			<p><a class="btn btn-secondary btn-sm btn_delete_item">Delete</a></p>
  		</div>
	</div>`;

	constructor ( items ) {
		this.items = items;
		this.init();
	}

	init () {
		if ( !this.get_elements() ) {
			return false;
		}

		this.list_items ();
		this.bind_events();
	};

	get_elements () {
		let $btn = jQuery( '#btn-add-gallery-item' );
		if ( $btn.length ) {
			this._l.set( 'btn_add', $btn );
		} else {
			return false;
		}

		let $items_list = jQuery( '.gallery-items-list' );
		if ( $items_list.length ) {
			this._l.set( 'list', $items_list );
		} else {
			return false;
		}

		this._l.set( 'aggregate_input', jQuery('input[name="meta[gallery_items]"]') );

		return true;
	};

	bind_events () {
		const _this = this;
		// Add button click
		_this._l.get( 'btn_add' ).click( function(e){
			e.preventDefault();
			_this.items.push({
				'img_url' : SSADMIN.HOME_URL + 'assets/images/slide-placeholder.png',
				'img_alt' : '',
				'title' : '',
				'subtitle' : '',
				'link_url' : '#',
				'link_text' : 'Go',
			});

			_this.list_items();
		});

		// Delete button click
		_this._l.get( 'list' ).on( 'click', '.btn_delete_item', function( e ) {
			e.preventDefault();
			let r = confirm( 'Are you sure you want to delete this?' );
			if ( !r ) {
				return false;
			}

			let index = jQuery(this).closest( '.single-item' ).data( 'index' );
			
			if ( _this.items.length > 0 ) {
				_this.items.splice( index, 1 );
			}

			_this.list_items();
		});

		// Keep saving the info as it is changed by user.
		// This is to avoid loosing any data when user clicks 'add new' or deletes an existing item
		_this._l.get( 'list' ).on( 'blur', '[data-field]', function(e){
			let $item = jQuery(this).closest( '.single-item' );

			// Update this entry 
			let new_details = {};
			$item.find( '[data-field]' ).each( function(){
				let $el = jQuery(this);
				new_details[ $el.data( 'field' ) ] = jQuery.trim( $el.val() );
			});

			_this.items[ $item.data( 'index' ) ] = new_details;
			_this._l.get( 'aggregate_input' ).val( JSON.stringify( _this.items ) );

			// Reset background image if image url entered is possibly an image
			let img_url = new_details.img_url;
			if ( !img_url ) {
				img_url = SSADMIN.HOME_URL + 'assets/images/slide-placeholder.png';
			} else {
				let is_valid_img = false;
				let extension = img_url.split( '.' ).pop();
				let img_extensions = [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp' ];
				if ( img_extensions.indexOf( extension ) != -1 ) {
					is_valid_img = true;
				}

				if ( !is_valid_img ) {
					img_url = SSADMIN.HOME_URL + 'assets/images/slide-placeholder.png';
				}
			}

			$item.find( 'img.card-img' ).attr( 'src', img_url );
		} );
	};

	list_items () {
		const _this = this;

		_this._l.get( 'list' ).html( '' );

		if ( this.items.length > 0 ) {
			for ( let i = 0; i < this.items.length; i++ ) {
				let template = this._template;
				
				template = template.replace( /{{{INDEX}}}/g, i );

				let img_url = this.items[i].hasOwnProperty( 'img_url' ) ? this.items[i].img_url : '';
				template = template.replace( /{{{IMG_URL}}}/g, img_url );

				let img_alt = this.items[i].hasOwnProperty( 'img_alt' ) ? this.items[i].img_alt : '';
				template = template.replace( /{{{IMG_ALT}}}/g, img_alt );

				let title = this.items[i].hasOwnProperty( 'title' ) ? this.items[i].title : '';
				template = template.replace( /{{{TITLE}}}/g, title );

				let subtitle = this.items[i].hasOwnProperty( 'subtitle' ) ? this.items[i].subtitle : '';
				template = template.replace( /{{{SUBTITLE}}}/g, subtitle );

				let link_url = this.items[i].hasOwnProperty( 'link_url' ) ? this.items[i].link_url : '';
				template = template.replace( /{{{LINK_URL}}}/g, link_url );

				let link_text = this.items[i].hasOwnProperty( 'link_text' ) ? this.items[i].link_text : '';
				template = template.replace( /{{{LINK_TEXT}}}/g, link_text );

				_this._l.get( 'list' ).append( template );
			}
		}

		_this._l.get( 'aggregate_input' ).val( JSON.stringify( _this.items ) );
	};
}