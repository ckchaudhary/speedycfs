<?php 
/**
 * IMPORTANT! Once setup is done, rename this file to something obscure.
 * Or better yet, take a backup of this file separately and then delete it completely from production site.
 */
include 'init.php';

include 'templates/parts/header.php';

$setup_completed = true;

$db = db();
$table_prefix = $db->table_prefix();
$db_details = unserialize( SSADMIN_DB );

$table_exists = $db->query( "SELECT count((1)) as 'ct' FROM INFORMATION_SCHEMA.TABLES where table_schema = ? and table_name = ?;", $db_details[ 'DB_NAME' ], table_prefix() . 'account' )->fetchVar();
if ( ! $table_exists ) {
	$setup_completed = false;
}

if ( !$setup_completed ) {
	echo '<h2 class="mt-3 mb-3">Starting setup..</h2>';

	// Create users table
	$db->query( 
		"CREATE TABLE IF NOT EXISTS {$table_prefix}account ( "
		. " id INT AUTO_INCREMENT PRIMARY KEY,"
		. " username VARCHAR(255) NOT NULL COMMENT 'username, used for logging in',"
		. " password VARCHAR(255) NOT NULL COMMENT 'md5ed password',"
		. " status VARCHAR(100) NULL DEFAULT 'active',"
		. " level TINYINT(2) UNSIGNED NULL DEFAULT 2 COMMENT '1: admin, 2: editor',"
		. " details TEXT NULL COMMENT 'can have a seriazlied array of information like full name, department etc',"
		. " created_at INT(10) NULL COMMENT 'timestamp(GMT) when this user was created'"
		. " ) ENGINE=INNODB;"
	);

	?>
	<div class="alert alert-secondary d-flex align-items-center" role="alert">
		<i class="bi bi-check-circle-fill"></i> &nbsp;
  		Account table check complete.
	</div>
	<?php 
	// Add an admin user, if none exists
	$admin_id = $db->query( "SELECT MAX(id) FROM {$table_prefix}account where status = 0 and level = 1;" )->fetchVar();
	if ( !$admin_id ) {
		$db->query( 
			"INSERT INTO {$table_prefix}account " 
			. " ( username, password, status, level, details, created_at ) "
			. " VALUES "
			. " ( 'webmaster', 'd634d7a75a57fa4e0476e56f9440fc1c', 'active', 1, 'User was added by setup script', ". time() ." ) "
		);

		$admin_id = $db->query( "SELECT MAX(id) FROM {$table_prefix}account where status = 0 and level = 1;" )->fetchVar();
	}

	?>
	<div class="alert alert-secondary d-flex" role="alert">
		<i class="bi bi-check-circle-fill"></i> &nbsp;
  		Admin account created.<br>
		Contact the developers to get username and password.
	</div>
	<?php 
	// Create the main content table
	$db->query( 
		"CREATE TABLE IF NOT EXISTS {$table_prefix}content ( "
		. " id INT AUTO_INCREMENT PRIMARY KEY,"
		. " type VARCHAR(255) NOT NULL COMMENT 'content type. E.g: \'page\', \'post\', \'media\', \'term\' etc',"
		. " subtype VARCHAR(255) NULL COMMENT 'content type. E.g: \'image\'( for media )',"
		. " identifier VARCHAR(255) NULL COMMENT 'E.g: url slug for blog posts',"
		. " title TEXT NULL COMMENT 'E.g: post title, file name, etc.',"
		. " details TEXT NULL COMMENT 'E.g: post content, page content etc.',"
		. " status VARCHAR(100) NULL DEFAULT 'active',"
		. " created_by INT NULL COMMENT 'id of user that created this ',"
		. " created_at INT(10) NULL COMMENT 'timestamp(GMT) when this entry was created'"
		. " ) ENGINE=INNODB;"
	);

	// Create the connections table
	$db->query( 
		"CREATE TABLE IF NOT EXISTS {$table_prefix}connection ( "
		. " id INT AUTO_INCREMENT PRIMARY KEY,"// primary key
		. " source VARCHAR(255) NULL COMMENT 'E.g: sub page id or invidividual image id in a gallery',"
		. " type VARCHAR(255) NULL COMMENT 'E.g: \'subpage\', or \'gallery_item\' ',"
		. " target VARCHAR(255) NULL COMMENT 'E.g: parent page id or gallery id',"
		. " created_at INT(10) NULL COMMENT 'timestamp(GMT) when this entry was created'"
		. " )  ENGINE=INNODB;"
	);

	// Create the meta table
	$db->query( 
		"CREATE TABLE IF NOT EXISTS {$table_prefix}meta ( "
		. " id INT AUTO_INCREMENT PRIMARY KEY,"
		. " object_id INT NOT NULL COMMENT 'content id or connection id or account id etc',"
		. " object_type VARCHAR(255) NOT NULL COMMENT '\'content\' or \'connection\' or \'account\' etc',"
		. " meta_key VARCHAR(255) NULL,"
		. " meta_value longtext NULL"
		. " )  ENGINE=INNODB;"
	);
	?>
	<div class="alert alert-secondary d-flex align-items-center" role="alert">
		<i class="bi bi-check-circle-fill"></i> &nbsp;
  		Database tables check complete.
	</div>
	<?php 
	// Add content
	
	// about us page
	$title = "About Us";
	$slug = slugify( $title );
	$db->query( 
		"INSERT INTO {$table_prefix}content " 
		. " ( type, subtype, identifier, title, details, created_by, created_at ) "
		. " VALUES "
		. " ( ?, ?, ?, ?, ?, ?, ? ) ",
		[ 'page', '', $slug, $title, 'hello world!', $admin_id, time() ]
	);

	// A slider
	$title = "Slideshow - Homepage";
	$slug = slugify( $title );
	$db->query( 
		"INSERT INTO {$table_prefix}content " 
		. " ( type, subtype, identifier, title, details, created_by, created_at ) "
		. " VALUES "
		. " ( ?, ?, ?, ?, ?, ?, ? ) ",
		[ 'gallery', 'slideshow', $slug, $title, 'This is the slide show used on home page.', $admin_id, time() ]
	);
	?>
	<div class="alert alert-secondary d-flex align-items-center" role="alert">
		<i class="bi bi-check-circle-fill"></i> &nbsp;
  		Sample data added.
	</div>
	
	<div class="alert alert-success" role="alert">
		<h4 class="alert-heading">All done.</h4>
		<p>
		Setup is complete. You are good to go!<br>
		If you are on production site, you must delete this file from file system, as it poses a security risk.<br>
		If you are not sure you are done yet, you should temporarily rename this file to something obscure.
		</p>
		<hr>
		<p class='mb-0'>If you are in development environment and need to run the setup again, go to your database and drop all tables</p>
	</div>
	<?php 
} else {
	?>
	<div class="alert alert-danger" role="alert">
		<h4 class="alert-heading">It appears that you've already run this setup.</h4>
		<p>
		If you are on production site, you must delete this file from file system, as it poses a security risk.<br>
		If you are not sure you are done yet, you should temporarily rename this file to something obscure.
		</p>
		<hr>
		<p class='mb-0'>If you are in development environment and need to run the setup again, go to your database and drop all tables</p>
	</div>
	<?php 
}

include 'templates/parts/footer.php';