<?php 
// root directory
define( 'ABSPATH', __DIR__ . '/' );

// Url of the admin panel
define( 'HOME_URL', 'https://speedycfs.local/ssadmin/' );

// Url of the public site
define( 'SITE_URL', 'https://speedycfs.local/' );

// sub directory, relative to root folder of the domain, in which the admin application resides.
define( 'SUBDIRECTORY', 'ssadmin' );

// sub directory for uploads folder, relative to root folder of the domain, in which the admin application resides.
define( 'UPLOADS_DIR', 'uploads' );

// time zone, in which you want to display date and time by default
define( 'TIME_ZONE', 'Asia/Kolkata' );

// database details
define ( 'SSADMIN_DB', serialize([
	'DB_HOST' 		=> 'localhost',
	'USER_NAME'		=> 'root',// database username/loginname 
	'USER_PASSWORD' => 'root',// password of the database user mentioned above

	'DB_NAME'		=> 'ssadmin_speedy',// Name of the database
	'TABLE_PREFIX' 	=> 'd1ee_',// Table prefix
]) );

// go to yourdomain.com/ssadmin/inc/tools/keygen.php and copy the generated key from there.
define ( 'SSADMIN_ENCRYPTION_SECRET', 'd87f320b77402dee6d7d1ee5cf9ef2068da41ad9' );

// List of API consumers we expect api requests coming from, anything else is discarded.
// After you add a new key here, you must also acquire an authentication key to send with all ajax requests.
// You can get the authentication key at: HOME_URL . 'ajax/api-new-consumer?id=NEW_CONSUMER_NAME' 
define( 'API_CONSUMERS', serialize([
	'speedy_local',
	'speedy_techarena_dev',
]) );