<?php 
namespace SSAdmin;

!defined('ABSPATH' ) ? exit() : '';

class GalleryListTable extends ListTable {
	protected $_content_type = '';

	public function __construct ( $args = '' ) {
		$this->_content_type = 'gallery';
		parent::__construct( $args );
	}

	public function columns () {
		return [
			//'id' => '#',
			'title' 	=> 'Title',
			'details' 	=> 'Description',
			'action' 	=> 'Action',
		];
	}

	public function output_column ( $column_id, $data_row ) {
		echo "<td class='{$column_id}'>";

		switch ( $column_id ) {
			case 'title':
				echo stripslashes( $data_row[ 'title' ] );
				break;

			case 'details':
				$details = stripslashes( $data_row[ 'details' ] );
				echo truncate_string( $details, 50, '...', true );
				break;

			case 'action':
				// Edit
				printf( "<a href='%s' class='btn btn-secondary'>Edit</a>&nbsp;", HOME_URL . 'edit/?c=' . $data_row[ 'id'] );
				break;
		}

		echo "</td>";
	}
}