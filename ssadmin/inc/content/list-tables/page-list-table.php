<?php 
namespace SSAdmin;

!defined('ABSPATH' ) ? exit() : '';

class PageListTable extends ListTable {
	public function __construct ( $args = '' ) {
		$this->_content_type = 'page';
		parent::__construct( $args );
	}

	public function columns () {
		return [
			'status' => '',
			//'id' => '#',
			'title' => 'Title',
			'action' => 'Action',
		];
	}

	public function output_column ( $column_id, $data_row ) {
		echo "<td class='{$column_id}'>";

		switch ( $column_id ) {
			case 'status':
				$status = $data_row['status'];
				if ( 'active' == $status ) {
					printf ( '<span title="%s" class="text-success"><i class="bi bi-check-circle-fill"></i></span>', ucfirst( $status ) );
				} else {
					printf ( '<span title="%s" class="text-muted"><i class="bi bi-pencil-square"></i></span>', ucfirst( $status ) );
				}
				break;

			case 'id':
				echo $data_row[ 'id' ];
				break;
			
			case 'title':
				echo stripslashes( $data_row[ 'title' ] );
				break;

			case 'action':
				echo "<div class='text-secondary'>";

				// View button
				printf( "<a href='%s' target='_blank' class='btn btn-secondary'>View<sup><i class='bi bi-box-arrow-in-up-right'></i></sup></a>&nbsp;", SITE_URL . $data_row[ 'identifier' ] );
				// Edit
				printf( "<a href='%s' class='btn btn-secondary'>Edit</a>&nbsp;", HOME_URL . 'edit/?c=' . $data_row[ 'id'] );
				
				echo "</div>";
				break;
		}

		echo "</td>";
	}
}