<?php 
namespace SSAdmin;

!defined('ABSPATH' ) ? exit() : '';

abstract class ListTable {
	protected $_config = [];
	protected $_items_per_page = 10;
	protected $_items = [];
	protected $_total_items = 0;

	protected $_content_type = '';
	protected $_capability = '';

	public function __construct ( $args = '' ) {
		$this->_capability = 'list_' . $this->_content_type;

		$defaults = [
			'pagination' => [
				'base_url' => '',
				'per_page' => 10,
			]
		];

		$this->_config = wp_parse_args( $args, $defaults );
	}

	#region data

	public function total_items () {
		return $this->_total_items;
	}

	protected function _set_total_count ( $num ) {
		$this->_total_items = $num;
	}

	public function get_items () {
		if ( empty( $this->_items ) ) {
			$this->_fetch_items();
		}

		return $this->_items;
	}

	protected function _fetch_items () {
		$db = db();

		$replacements_count = [];
		$replacements_rows = [];

		$where = [ '1=1' ];

		// Only Media
		$where[] = "c.type = ?";
		$replacements_count[] = $this->_content_type;
		$replacements_rows[] = $this->_content_type;
		
		// Search term
		if ( isset( $_GET['s'] ) ) {
			$where[] =  'c.title LIKE ?';
			$replacements_count[] = '%' . $_GET['s'] . '%';
			$replacements_rows[] = '%' . $_GET['s'] . '%';
		}

		$select_count = "SELECT COUNT( c.id ) ";
		$select_rows = "SELECT c.* ";

		$query_count = "{$select_count} FROM {$db->table_prefix()}content c WHERE " . implode( " AND ", $where );
		$this->_set_total_count( $db->query( $query_count, $replacements_count )->fetchVar() );

		$query_rows = "{$select_rows} FROM {$db->table_prefix()}content c WHERE " . implode( " AND ", $where );
		$query_rows .= " ORDER BY c.created_at DESC ";
		$query_rows .= " " . $this->_get_limit_query();

		$this->_items = $db->query( $query_rows, $replacements_rows )->fetchAll();
	}

	protected function _get_limit_query () {
		$per_page = $this->_items_per_page;
		$current_page = isset( $_GET['list'] ) && absint( $_GET['list'] ) > 0 ? absint( $_GET['list'] ) : 1;
		$offset = ( $current_page - 1 ) * $per_page;

		return "LIMIT {$offset}, $per_page";
	}

	#endregion

	#region Layout

	public function output () {
		if ( !user_can( get_current_user_id(), $this->_capability ) ) {
			get_template_part( 'parts/access-denied' );
			return false;
		}

		// fetch items
		$this->_fetch_items ();
		?>
		<form method='get'>
			<div class="container px-1 mb-4">
				<?php $this->output_search_form();?>
			</div>

			<?php $this->output_pagination();?>

			<?php $this->output_table_header();?>

			<?php $this->output_rows();?>

			<?php $this->output_table_footer();?>
		</form>
		<?php 
	}

	public abstract function columns ();

	public function output_search_form () {
		?>
		<div class="input-group mb-3">
			<input type="search" name='s' value='<?php echo isset( $_GET['s'] ) ? esc_attr( $_GET['s'] ) : '';?>' class="form-control" placeholder="Search by name" aria-label="Search term" aria-describedby="button-search">
			<button class="btn btn-outline-secondary" type="submit" id="button-search"><i class="bi bi-search"></i></button>
		</div>
		<?php 
	}

	public function output_pagination () {
		$total_items = $this->total_items();
		if ( ! $total_items ) { 
			return;
		}

		$per_page = $this->_items_per_page;
		$current_page = isset( $_GET['list'] ) && absint( $_GET['list'] ) > 0 ? absint( $_GET['list'] ) : 1;
		$start_at = ( ( $current_page - 1 ) * $per_page ) + 1;
		$end_at = ( $start_at + $per_page - 1 ) > $total_items ? $total_items : ( $start_at + $per_page - 1 );
		$enable_previous = $start_at > 1;
		$enable_next = $end_at < $total_items;

		$url_parts = parse_url( $_SERVER['REQUEST_URI'] );
		$url_parts[ 'path' ] = isset( $url_parts[ 'path' ] ) ? trim( $url_parts[ 'path' ], '/' ) : '';
		if ( isset( $url_parts[ 'path' ] ) && !empty( $url_parts[ 'path' ] ) ) {
			// If the admin resides in a subdirectory, dont consider that subdirectory for page name
			if ( SUBDIRECTORY ) {
				$sub_directory = trim( SUBDIRECTORY, '/' );
				if ( strpos( $url_parts[ 'path' ], $sub_directory ) === 0 ) {
					$url_parts[ 'path' ] = substr( $url_parts[ 'path' ], strlen( $sub_directory ) );
					$url_parts[ 'path' ] = trim( $url_parts[ 'path' ], '/' );
				}
			}
		}

		$params = isset( $url_parts['query'] ) ? explode( '&', $url_parts['query'] ) : [];
		if ( !empty( $params ) ) {
			$temp = [];
			foreach ( $params as $kv ) {
				$k_v = explode( '=', $kv );
				if ( $k_v[ 0 ] == 'list' ) {
					continue;
				}

				$temp[] = $kv;
			}
			$params = $temp;
		}
		$pagination_base_url = HOME_URL . $url_parts[ 'path' ] . '?' . implode( '&', $params );
		?>
		<div class="d-flex flex-row-reverse text-secondary">
  			<div class="p-2">
			  	<nav>
					<ul class="pagination justify-content-end pagination-sm">
						<li class="page-item <?php if ( ! $enable_previous ) { echo 'disabled'; }?>">
							<?php $url = $enable_previous ? $pagination_base_url . '&list=' . ( $current_page - 1 ) : '#';?>
							<a class="page-link" href="<?php echo esc_attr( $url );?>">Previous</a>
						</li>
						<li class="page-item <?php if ( ! $enable_next ) { echo 'disabled'; }?>">
							<?php $url = $enable_next ? $pagination_base_url . '&list=' . ( $current_page + 1 ) : '#';?>
							<a class="page-link" href="<?php echo esc_attr( $url );?>">Next</a>
						</li>
					</ul>
				</nav>
			</div>

			<div class="p-2">
				<?php echo "Showing $start_at to $end_at of $total_items items.";?>
			</div>
		</div>
		<?php 
	}

	public function output_table_header () {
		?>
		<table class="table table-striped item-list-table">
		<thead>
			<tr class="fs-4">
				<?php 
				foreach ( $this->columns() as $column_id => $column_name ) { 
					printf( "<th scope='col' class='%s'>%s</th>", $column_id, $column_name );
				}
				?>
			</tr>
		</thead>
		<?php 
	}

	public function output_table_footer () {
		$total_items = $this->total_items();
		if ( $total_items ) { 
			?>
			<tfoot>
				<tr>
					<td colspan="100%">
						<?php $this->output_pagination();?>
					</td>
				</tr>
			</tfoot>
			<?php 
		}
		echo "</table>";
	}

	public function output_rows () {
		echo "<tbody>";
		if ( $this->total_items() ) { 
			foreach ( $this->get_items() as $row ) {
				$this->output_row ( $row );
			}
		} else {
			echo "<tr class='row-no-items'><td colspan='100%'>No items found.</td></tr>";
		}
		echo "</tbody>";
	}

	public function output_row ( $row ) {
		echo "<tr data-item_id='{$row['id']}'>";
		foreach ( $this->columns() as $column_id => $column_name ) {
			$this->output_column( $column_id, $row );
		}
		echo "</tr>";
	}

	public abstract function output_column ( $column_id, $data_row );

	#endregion
}