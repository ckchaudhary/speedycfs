<?php 
namespace SSAdmin;

!defined('ABSPATH' ) ? exit() : '';

class MediaListTable extends ListTable {
	public function __construct ( $args = '' ) {
		$this->_content_type = 'media';
		parent::__construct( $args );
	}

	public function columns () {
		return [
			//'id' => '#',
			'preview' => '',
			'title' => 'Title',
			'date' => 'Date',
			'action' => 'Action',
		];
	}

	public function output_column ( $column_id, $data_row ) {
		echo "<td class='{$column_id}'>";

		$upload_relative_url = _get_meta( 'media', $data_row['id'], 'relative_url' );

		switch ( $column_id ) {
			case 'preview':
				echo get_media_preview( $data_row[ 'id' ] );
				break;

			case 'id':
				echo $data_row[ 'id' ];
				break;
			
			case 'title':
				echo stripslashes( $data_row[ 'title' ] );
				break;

			case 'date':
				echo date( DATE_FORMAT, $data_row[ 'created_at' ] ) . '&nbsp;<small>' . date( TIME_FORMAT, $data_row[ 'created_at' ] )  . '</small>';
				break;

			case 'action':
				// View button
				printf( "<a href='%s' target='_blank' class='btn btn-secondary'>View <sup><i class='bi bi-box-arrow-in-up-right'></i></sup></a>&nbsp;", SITE_URL . UPLOADS_DIR . '/' . $upload_relative_url ) .
				// Copy url
				printf( "<a href='#' class='btn btn-secondary' data-copy_text data-text_to_copy='%s'>Copy url</a>&nbsp;", SITE_URL . UPLOADS_DIR . '/' . $upload_relative_url );
				// Delete
				printf( "<a href='%s' class='btn btn-danger btn_delete_item'>Delete</a>&nbsp;", esc_attr( get_delete_url( $data_row[ 'id' ] ) ) );
				break;
		}

		echo "</td>";
	}
}