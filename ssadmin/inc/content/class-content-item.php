<?php 
namespace SSAdmin;

!defined('ABSPATH' ) ? exit() : '';

class ContentItem {
	#region bootstrap
	protected $_data = [];

	public function __construct ( $id = false ) {
		if ( $id ) {
			$this->id = $id;
			$this->_fetch_details();
		}
	}

    public function __get ( $name ) {
        if ( !array_key_exists( $name, $this->_data ) ) {
            return null;
        }

		$val = $this->_data[ $name ];
		switch ( $name ) {
			case 'type':
				if ( empty( $val ) ) {
					$val = 'blog';// default content type
				}
				break;

			case 'status':
				if ( empty( $val ) ) {
					if ( 'blog' == $this->type ) {
						$val = 'draft';
					} else {
						$val = 'active';
					}
				}
				break;
		}

        return $val;
    }

	public function __set ( $name, $value ) {
		switch ( $name ) {
			case 'id':
			case 'created_at':
			case 'created_by':
				$value = absint( $value );
				break;

			case 'details':
				$value = sanitize_html_textarea_field( $value );
				break;

			case 'status':
				$allowed = [ 'active', 'draft' ];
				if ( ! in_array( $value, $allowed ) ) {
					$value = $this->status;// default value
				}
			
			default: 
				$value = sanitize_text_field( $value );
				break;

		}

        $this->_data[ $name ] = $value;
    }

    public function __isset ( $name ) {
        return isset( $this->_data[ $name ] );
    }

    public function __unset( $name ) {
        unset( $this->_data[ $name ] );
    }

	#endregion

	#region Fetch data

	protected function _fetch_details () {
		$db = db();
		$db_row = $db->query( "SELECT * FROM {$db->table_prefix()}content WHERE id = ?", $this->id )->fetchArray();
		if ( !empty( $db_row ) ) {
			foreach ( $db_row as $k => $v ) {
				$this->_data[ $k ] = maybe_unserialize( $v );
			}
		} else {
			$this->_data = [];// set empty
		}
	}

	public function get_meta ( $meta_key ) {
		return _get_meta( $this->type, $this->id, $meta_key );
	}

	public function update_meta ( $meta_key, $meta_value ) {
		return _update_meta( $this->type, $this->id, $meta_key, $meta_value );
	}

	#endregion

	#region Create new

	public function create () {
		$validatated = $this->_validate_add();

		if ( 'success' !== $validatated['status'] ) {
			// Some error occured
			add_notice( $validatated['data']['message'], [ 'type' => 'danger' ] );
			return false;
		}

		// Insert new record
		$db = db();
		$db->query( 
			"INSERT INTO {$db->table_prefix()}content ( type, identifier, title, status, created_by, created_at ) VALUES ( ?, ?, ?, ?, ?, ? )",
			[
				$this->type,
				$this->identifier,
				$this->title,
				'draft',
				get_current_user_id(),
				time()
			]
		);

		$insert_id = $db->lastInsertID();
		if ( !empty( $insert_id ) ) {
			$this->id = $insert_id;
			// fetch, to retrieve default values from database for empty fields, if any.
			$this->_fetch_details();
		}

		return $this->id;
	}

	protected function _validate_add () {
		if ( !is_user_logged_in() ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'You must be logged in to perform this action.',
					'err_type' => 'no_guest_access',
				],
			];
		}

		if ( null !== $this->id ) {
			return [
				'status' => 'error',
				'data' => [
					'err_type' => 'invalid_operation',
					'message' => 'This item already exists.',
				],
			];
		}

		if ( empty( $this->title ) ) {
			return [
				'status' => 'error',
				'data' => [
					'err_type' => 'invalid_data',
					'message' => 'Title can\'t be empty.',
				],
			];
		}

		if ( empty( $this->type ) ) {
			return [
				'status' => 'error',
				'data' => [
					'err_type' => 'invalid_data',
					'message' => 'Invalid content type.',
				],
			];
		}

		if ( !user_can( get_current_user_id(), 'add_' . $this->type ) ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Your account does not have access to perform this action.',
					'err_type' => 'access_restricted',
				],
			];
		}

		$this->identifier = $this->_determine_identifier( $this->title, $this->type );

		return [ 'status' => 'success' ];
	}

	#endregion

	#region Update

	public function maybe_update () {
		if ( ! isset( $_POST['update_content'] ) ) {
			return false;
		}

		// check nonce
		$nonce = isset( $_POST['_nonce'] ) ? $_POST['_nonce'] : '';
		if ( empty( $nonce ) || !verify_nonce( $nonce, 'edit-content' ) ) {
			// Some error occured
			add_notice( 'Invalid security token.', [ 'type' => 'danger' ] );
			return false;
		}

		$new_title = isset( $_POST['title'] ) ? sanitize_text_field( $_POST['title'] ) : '';
		$new_identifier = isset( $_POST['identifier'] ) ? sanitize_text_field( $_POST['identifier'] ) : '';
		$new_details = isset( $_POST['details'] ) ? sanitize_html_textarea_field( $_POST['details'] ) : '';
		$new_status = isset( $_POST['status'] ) ? sanitize_text_field( $_POST['status'] ) : '';

		$new_data = [
			'id' => isset( $_POST['content_id'] ) ? absint( $_POST['content_id'] ) : 0,
			'type' => $this->type,
			'title' => $new_title,
			'identifier' => $new_identifier,
			'details' => $new_details,
			'status' => $new_status,
		];

		$validatated = $this->_validate_update( $new_data );
		if ( 'success' !== $validatated[ 'status' ] ) {
			// Some error occured
			add_notice( $validatated['data']['message'], [ 'type' => 'danger' ] );
			return false;
		}

		// Update with sanitized data
		foreach ( $validatated['data'] as $k => $v ) {
			$this->__set( $k, $v );
		}

		$this->update();

		$this->maybe_update_metas();
		add_notice( 'Updated!', [ 'type' => 'success' ] );
	}

	/**
	 * Update the properties of current object into databse.
	 * 
	 * @return void
	 */
	public function update () {
		$db = db();

		// Update
		$db->query( 
			"UPDATE {$db->table_prefix()}content SET type = ?, subtype = ?, identifier = ?, title = ?, details = ?, status = ? WHERE id = ?", 
			[
				$this->type,
				$this->subtype,
				$this->identifier,
				$this->title,
				$this->details,
				$this->status,
				$this->id,
			]
		);
	}

	protected function _validate_update ( $raw_data) {
		if ( !is_user_logged_in() ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'You must be logged in to perform this action.',
					'err_type' => 'no_guest_access',
				],
			];
		}

		if ( empty( $raw_data[ 'id' ] ) || $raw_data[ 'id' ] !== $this->id ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Invalid action.',
					'err_type' => 'id_mismatch',
				],
			];
		}

		if ( !\user_can( \get_current_user_id(), 'update_' .  $this->type ) ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Your account does not have access to perform this action.',
					'err_type' => 'access_restricted',
				],
			];
		}

		// Status must be one of allowed values
		if ( !in_array( $raw_data[ 'status' ], [ 'active', 'draft' ] ) ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Invalid data.',
					'err_type' => 'invalid_status',
				],
			];
		}

		if ( $raw_data[ 'status' ] == 'active' ) {
			// Title can't be empty
			if ( empty( $raw_data[ 'title' ] ) ) {
				return [
					'status' => 'error',
					'data' => [
						'message' => 'Title can\'t be empty.',
						'err_type' => 'invalid_data',
					],
				];	
			}

			// Details can't be empty
			if ( empty( $raw_data[ 'details' ] ) ) {
				return [
					'status' => 'error',
					'data' => [
						'message' => 'Details can\'t be empty.',
						'err_type' => 'invalid_data',
					],
				];	
			}
		}

		// Must have a valid identifier
		if ( !$this->identifier_is_valid( $raw_data[ 'identifier' ], $this->type, $this->id ) ) {
			$raw_data[ 'identifier' ] = $this->_determine_identifier( $raw_data[ 'title' ], $this->type );
		}

		return [ 'status' => 'success', 'data' => $raw_data ];
	}

	/**
	 * If the $key exist in $_POST, use that, use $this->key instead.
	 * 
	 * @return mixed
	 */
	public function form_data ( $key ) {
		return isset( $_POST[ $key ] ) ? $_POST[ $key ] : $this->__get( $key );
	}

	public function maybe_update_metas () {
		$meta_fields = $this->get_meta_fields_for_editor();
		if ( !empty( $meta_fields ) ) {
			$posted_meta = isset( $_POST['meta'] ) && !empty( $_POST['meta'] ) ? $_POST['meta'] : [];
			foreach ( $meta_fields as $field_name => $field_details ) {
				$posted_val = isset( $posted_meta[ $field_name ] ) ? $posted_meta[ $field_name ] : $field_details[ 'default_val' ];
				if ( isset( $field_details[ 'sanitization' ] ) && !empty( $field_details[ 'sanitization' ] ) ) {
					$posted_val = call_user_func( $field_details[ 'sanitization' ], $posted_val );
				}

				$this->update_meta( $field_name, $posted_val );
			}
		}
	}

	public function get_meta_fields_for_editor () {
		$meta_fields = [];

		if ( 'gallery' == $this->type ) {
			$meta_fields[ 'gallery_items' ] = [
				'type' 			=> 'hidden',
				'default_val'	=> '',
				'sanitization' 	=> 'sanitize_text_field'
			];
		}

		return $meta_fields;
	}

	#endregion

	#region Delete

	public function delete () {
		if ( ! user_can( get_current_user_id(), 'delete_' . $this->type ) ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Your account does not have access to perform this action.',
					'err_type' => 'access_restricted',
				],
			];
		}

		$db = db();
		// @todo: delete from connections

		// delete file from filesystem
		if ( 'media' == $this->type ) {
			$relative_path = $this->get_meta( 'relative_path' );
			if ( $relative_path ) {
				$full_path = dirname( ABSPATH, 1 ) . DIRECTORY_SEPARATOR . UPLOADS_DIR . DIRECTORY_SEPARATOR . $relative_path;
				unlink( $full_path );
			}
		}

		// Delete from meta
		$db->query( "DELETE FROM {$db->table_prefix()}meta WHERE object_id = ? AND object_type = ?", [ $this->id, $this->type ] );

		// Finally, delete from content
		$db->query( "DELETE FROM {$db->table_prefix()}content WHERE id = ? AND type = ?", [ $this->id, $this->type ] );

		return [ 'status' => 'success' ];
	}

	#endregion

	#region helpers

	public function identifier_is_valid ( $identifier, $content_type, $content_id ) {
		if ( empty( $identifier ) ) {
			return false;
		}

		$db = db();
		$existing = $db->query( "SELECT id FROM {$db->table_prefix()}content WHERE type = ? AND identifier = ? LIMIT 0, 2", [ $content_type, $identifier ] )->fetchAll();
		if ( !empty( $existing ) ) {
			$is_unique = true;
			var_dump( $existing );
			foreach ( $existing as $row ) {
				if ( $row[ 'id' ] !== $content_id ) {
					$is_unique = false;
					break;
				}
			}
		} else {
			$is_unique = true;
		}

		return $is_unique;
	}
	
	public function _determine_identifier ( $title, $content_type) {
		if ( !$title ) {
			return false;
		}

		$is_found = false;
		$identifier = slugify( $title );
		// Restrict to 50 characters, to keep a reasonable limit on url length
		$identifier = truncate_string( $identifier, 50 );

		$db = db();
		$existing = $db->query( "SELECT identifier FROM {$db->table_prefix()}content WHERE type = ? AND identifier = ? ", [ $content_type, $identifier ] )->fetchVar();
		if ( !$existing ) {
			// This is unique already.
			return $identifier;
		}

		// Another entry with this identifier already exists.
		// Add suffixes and try
		$counter = 1;
		do {
			$new_identifier = $identifier . '-' . $counter;
			$existing = $db->query( "SELECT identifier FROM {$db->table_prefix()}content WHERE type = ? AND identifier = ? ", [ $content_type, $new_identifier ] )->fetchVar();
			if ( !$existing ) {
				$is_found = $new_identifier;// This is unique, yet.
			}

			$counter++;

			// Lets ony try till 10
			if ( $counter > 10 ) {
				break;
			}
		} while ( ! $is_found );

		if ( $is_found ) {
			return $is_found;
		}

		// If still not found, then simply add the timestamp as suffix
		return $identifier . '_' . str_replace( ' ', '', microtime() );
	}

	#endregion

	#region sanitization

	protected function _sanitize ( $field, $value ) {
		// for post_content and excerpt 
		// First wp_slash is used and then following filters are applied
		// 
		// convert_invalid_entities
		// balanceTags
		// wp_targeted_link_rel
		// wp_filter_post_kses
		// wp_filter_global_styles_post

		// for title 
		// First wp_slash is used and then following filters are applied
		// 
		// trim
		// wp_targeted_link_rel
		// wp_filter_kses
		// 
		// Finally wp_unslash is used.

		switch ( $field ) {
			case 'title':
				$value = wp_slash( $value );
				break;

			case 'details':
				$value = wp_slash( $value );
				$value = convert_invalid_entities( $value );
				break;
		}

		return $value;
	}

	#endregion

}