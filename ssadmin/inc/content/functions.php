<?php 
!defined('ABSPATH' ) ? exit() : '';

function get_media_preview ( $media_content_id ) {
	$relative_url = _get_meta( 'media', $media_content_id, 'relative_url' );
	$name_parts = explode( '.', $relative_url );
	$extension = strtolower( end( $name_parts ) );
	if ( empty( $extension ) ) {
		return '<i class="bi bi-file"></i>';
	}

	$file_types = [
		'image' => [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp' ],
		'pdf' => [ 'pdf' ],
		'word' => [ 'doc', 'docx' ],
		'spreadsheet' => [ 'xls', 'xlsx' ],
		'ppt' => [ 'ppt', 'pptx' ],
		'zip' => [ 'zip' ],
	];

	$current_type = '';
	foreach ( $file_types as $type => $extensions ) {
		if ( in_array( $extension, $extensions ) ) {
			$current_type = $type;
			break;
		}
	}

	if ( 'image' == $current_type ) {
		return "<img src='" . SITE_URL . UPLOADS_DIR . '/' . $relative_url . "' >";
	}

	$icons = [
		'pdf' => '<i class="bi bi-file-pdf"></i>',
		'word' => '<i class="bi-file-word"></i>',
		'spreadsheet' => '<i class="bi-file-excel"></i>',
		'ppt' => '<i class="bi-file-ppt"></i>',
		'zip' => '<i class="bi-file-zip"></i>',
	];

	return isset( $icons[ $current_type ] ) ? $icons[ $current_type ] : '<i class="bi bi-file"></i>';
}

function get_delete_url ( $content_id ) {
	return HOME_URL . 'ajax/delete-content/?c=' . $content_id . '&_nonce=' . create_nonce( 'delete_content' );
}