<?php 
!defined( 'ABSPATH' ) ? exit() : '';

function header_metas () {
	?>
	<meta charset="<?php echo SITE_CHARSET; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php 
	$title = current_request()->get_details( 'title' );
	echo "<title>". stripslashes( $title ) ."</title>";
}

function header_assets () {
	?>
	<!-- Bootstrap CSS -->
	<?php /*<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">*/?>
	<link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/bootstrap-pulse/bootstrap.min.css">

	<!-- Bootstrap Icons -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

	<link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/main.css">
	<?php 
	$page_names = current_request()->get_path_parts();
	if ( !empty( $page_names ) && 'edit' == $page_names[0] ) {
		//?><link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"><?php 
		?><link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet"><?php 
	}
}

function footer_assets () {
	?>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script>
	var SSADMIN = {
		HOME_URL : '<?php echo HOME_URL;?>',
		SITE_URL : '<?php echo SITE_URL;?>',
	};
	</script>
	<script src="<?php echo HOME_URL;?>assets/js/utils.js"></script>
	<script src="<?php echo HOME_URL;?>assets/js/common.js"></script>
	<?php 
	$page_names = current_request()->get_path_parts();
	if ( !empty( $page_names ) && 'edit' == $page_names[0] ) {
		// <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
		?>
		<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
		<script src="<?php echo HOME_URL;?>assets/js/editor.js"></script>
		<?php 
	}

	if ( !empty( $page_names ) && 'media' == $page_names[0] ) {
		?>
		<script src="<?php echo HOME_URL;?>assets/js/jquery.form.min.js"></script>
		<script src="<?php echo HOME_URL;?>assets/js/file-uploader.js"></script>
		<?php 
	}
}

function body_class () {
	$body_classes = current_request()->get_details( 'body_classes' );
	if ( $body_classes ) {
		echo implode( ' ', $body_classes );
	}
}