<?php 
namespace LogiPrism\SS;

class Request {
	#region bootstrap

	// Information parsed from url
	protected $_request = [];

	// Information supplied by templates
	protected $_current = [];

	private function __construct() {
	}

	public static function instance() {
		static $instance;
		if ( ! isset( $instance ) ) {
			$instance = new self();
		}
		return $instance;
	}

	#endregion

	#region Current request details
	
	public function parse () {
		$parts = parse_url( $_SERVER['REQUEST_URI'] );
		$path = isset( $parts['path'] ) ? trim( $parts['path'], '/' ) : '';
		if ( $path ) {
			// If the admin resides in a subdirectory, dont consider that subdirectory for page name
			if ( SUBDIRECTORY ) {
				$sub_directory = trim( SUBDIRECTORY, '/' );
				if ( strpos( $path, $sub_directory ) === 0 ) {
					$path = substr( $path, strlen( $sub_directory ) );
					$path = trim( $path, '/' );
				}
			}
		}

		if ( $path ) {
			$this->_request[ 'pagenames' ] = explode( '/', $path );
		}

		$this->_request[ 'params' ] = [];
		$qs = isset( $parts['query'] ) ? $parts['query'] : '';
		if ( $qs ) {
			$qs = explode( '&', $qs );
			foreach ( $qs as $k => $v ) {
				$this->_request[ 'params' ][ $k ] = $v;
			}
		}
	}

	public function get_path_parts () {
		return isset( $this->_request['pagenames'] ) ? $this->_request[ 'pagenames' ] : [];
	}

	public function templates_to_load () {
		$pagenames = $this->get_path_parts();
		if ( !empty( $pagenames ) && 'api' == $pagenames[0] ) {
			return [ 'api' ];
		}

		// All other templates are only accessible to loggedin users.
		if ( !is_user_logged_in() ) {
			// all admin pages are login protected
			return [ 'authenticate' ];
		}
		
		if ( empty( $pagenames ) ) {
			return [ 'home' ];
		}

		$last_path = '';
		$templates_to_load = [];
		foreach ( $pagenames as $pagename ) {
			if ( !$last_path ) {
				$last_path = $pagename;
			} else {
				$last_path .= '/' . $pagename;
			}

			$templates_to_load[] = $last_path;
		}

		if ( count( $templates_to_load ) > 1 ) {
			$templates_to_load = array_reverse( $templates_to_load );
		}

		return $templates_to_load;
	}

	#endregion

	#region Current page details

	public function get_details( $key = '' ) {
		if ( $key ) {
			if ( isset( $this->_current[$key] ) ) {
				return $this->_current[$key];
			} else {
				return null;
			}
		}
		
		return $this->_current;
	}

	public function set_details( $details ) {
		if ( empty( $details ) ) {
			return false;
		}

		foreach ( $details as $key => $val ) {
			// @todo: perform checks for special info
			$this->_current[$key] = $val;
		}
	}

	#endregion
}