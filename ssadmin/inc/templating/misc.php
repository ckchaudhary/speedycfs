<?php 
!defined( 'ABSPATH' ) ? exit() : '';

function add_notice ( $html, $args = '' ) {
	$defaults = [
		'is_dismissable' => true,
		'type' => 'warning',
	];

	$args = wp_parse_args( $args, $defaults );

	if ( $args['type'] == 'error' ) {
		$args['type'] = 'danger';// bootstrap class
	}

	global $ssadmin_notices;
	if ( empty( $ssadmin_notices ) ) {
		$ssadmin_notices = [];
	}

	$ssadmin_notices[] = [ 'type' => $args['type'], 'html' => $html, 'args' => $args ];
}

function _print_notices () {
	global $ssadmin_notices;
	if ( empty( $ssadmin_notices ) ) {
		return false;
	}

	foreach ( $ssadmin_notices as $notice ) {
		$is_dismissable = isset( $notice['args'][ 'is_dismissable' ] ) && $notice['args'][ 'is_dismissable' ];

		printf( 
			"<div class='alert alert-%s %s' role='alert'>%s %s</div>",
			$notice[ 'type' ],
			$is_dismissable ? 'alert-dismissible fade show' : '',
			$notice[ 'html' ],
			$is_dismissable ? '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' : '',
		);
	}

	$ssadmin_notices = [];
}

function get_template_part( $slug, $name = null, $args = array() ) {
    $templates = array();
    $name      = (string) $name;
    if ( '' !== $name ) {
        $templates[] = "{$slug}-{$name}.php";
    }
 
    $templates[] = "{$slug}.php";

    if ( ! locate_template( $templates, true, false, $args ) ) {
        return false;
    }

	return true;
}

function locate_template( $template_names, $load = false, $require_once = true, $args = array() ) {
    $located = '';
    foreach ( (array) $template_names as $template_name ) {
        if ( ! $template_name ) {
            continue;
        }

        if ( file_exists( TEMPLATE_PATH . $template_name ) ) {
            $located = TEMPLATE_PATH . $template_name;
            break;
        }

		if ( file_exists( ABSPATH . 'templates/' . $template_name ) ) {
            $located = ABSPATH . 'templates/' . $template_name;
            break;
        }
    }
 
    if ( $load && '' !== $located ) {
        if ( $require_once ) {
			require_once $located;
		} else {
			require $located;
		}
    }
 
    return $located;
}

function redirect( $url, $statusCode = 303 ){
	header( 'Location: ' . $url, true, $statusCode );
   	die();
}