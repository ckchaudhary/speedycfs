<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// Content type blog
define( 'CT_BLOG', 'blog' );

// Helper functions
require_once ABSPATH . 'inc/content/functions.php';

// Load classes for all list tables
require_once ABSPATH . 'inc/content/list-tables/list-table.php';
require_once ABSPATH . 'inc/content/list-tables/media-list-table.php';
require_once ABSPATH . 'inc/content/list-tables/page-list-table.php';
require_once ABSPATH . 'inc/content/list-tables/post-list-table.php';
require_once ABSPATH . 'inc/content/list-tables/gallery-list-table.php';

// Load classes for all content types
require_once ABSPATH . 'inc/content/class-content-item.php';

/**
 * Get the meta value for given meta key
 * 
 * @param string 	$object_type 	Type of object. E.g: 'content', 'account' or 'connection'
 * @param int 		$object_id 		
 * @param string 	$meta_key 
 * 
 * @return mixed If found, null otherwise.
 */
function _get_meta ( $object_type, $object_id, $meta_key ) {
	// First try to fetch from cache
	$cache_group = $object_type . '_' . $object_id;
	$meta_val = cache()->get( $meta_key, $cache_group );
	if ( null === $meta_val ) {
		// If not found, query database
		$db = db();
		$meta_val = $db->query( "SELECT meta_value FROM {$db->table_prefix()}meta WHERE object_type = ? AND object_id = ? AND meta_key = ?", [ $object_type, $object_id, $meta_key ] )->fetchVar();
		if ( !empty( $meta_val ) ) {
			// Update into cache
			cache()->update( $meta_key, $meta_val, $cache_group );
		}
	}

	if ( !empty( $meta_val ) ) {
		$meta_val = maybe_unserialize( $meta_val );
		return $meta_val;
	}

	return null;
}

/**
 * Update the value for given meta key for given object.
 * 
 * @param string $object_type 
 * @param int $object_id 
 * @param string $meta_key 
 * @param mixed $meta_val
 * 
 * @return void
 */
function _update_meta ( $object_type, $object_id, $meta_key, $meta_val ) {
	$meta_val = maybe_serialize( $meta_val );

	$db = db();
	$existing_entry_id = $db->query( "SELECT id FROM {$db->table_prefix()}meta WHERE object_type = ? AND object_id = ? AND meta_key = ?", [ $object_type, $object_id, $meta_key ] )->fetchVar();
	if ( $existing_entry_id ) {	
		$db->query( "UPDATE {$db->table_prefix()}meta SET meta_value = ? WHERE id = ?", [ $meta_val, $existing_entry_id ] );
	} else {
		$db->query( "INSERT INTO {$db->table_prefix()}meta ( object_id, object_type, meta_key, meta_value ) VALUES ( ?, ?, ?, ? )", [ $object_id, $object_type, $meta_key, $meta_val ] );
	}

	// Update into cache
	$cache_group = $object_type . '_' . $object_id;
	cache()->update( $meta_key, $meta_val, $cache_group );
}