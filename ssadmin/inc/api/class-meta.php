<?php 
namespace SSAdmin\API;

!defined('ABSPATH' ) ? exit() : '';

class Meta extends EndPoint {
	protected function _output() {
		$meta_key = isset( $_GET['meta_key'] ) ? sanitize_text_field( $_GET[ 'meta_key' ] ) : '';
		if ( !$meta_key ) {
			$this->_retval[ 'status' ][ 'error' ];
			$this->_retval[ 'data' ][ 'message' ] = 'Invalid input.';
			$this->_retval[ 'data' ][ 'err_type'] = 'invalid_input';
			return $this->_retval;
		}

		$content_type = isset( $_GET['content_type'] ) ? $_GET['content_type'] : '';
		if ( !empty( $content_type ) ) {
			if ( !$this->_can_fetch_meta_for_content_type( $content_type ) ) {
				$this->_retval[ 'status' ][ 'error' ];
				$this->_retval[ 'data' ][ 'message' ] = 'Access denied.';
				$this->_retval[ 'data' ][ 'err_type'] = 'restricted_content_type';
				return $this->_retval;
			}
		}

		$content_id = isset( $_GET[ 'content_id' ] ) && absint( $_GET[ 'content_id'] ) > 0 ? absint( $_GET[ 'content_id' ] ) : '';
		if ( empty( $content_id ) ) {
			$this->_retval[ 'status' ][ 'error' ];
			$this->_retval[ 'data' ][ 'message' ] = 'Invalid input.';
			$this->_retval[ 'data' ][ 'err_type'] = 'invalid_input';
			return $this->_retval;
		}

		$content_obj = new \SSAdmin\ContentItem( $content_id );
		if ( ! $content_obj->id || 'active' !== $content_obj->status ) {
			$this->_retval[ 'status' ][ 'error' ];
			$this->_retval[ 'data' ][ 'message' ] = 'Invalid input.';
			$this->_retval[ 'data' ][ 'err_type'] = 'invalid_input';
			return $this->_retval;
		}

		// Verify content type again
		if ( !$this->_can_fetch_meta_for_content_type( $content_obj->type ) ) {
			$this->_retval[ 'status' ][ 'error' ];
			$this->_retval[ 'data' ][ 'message' ] = 'Access denied.';
			$this->_retval[ 'data' ][ 'err_type'] = 'restricted_content_type';
			return $this->_retval;
		}

		$this->_retval[ 'data' ] = $content_obj->get_meta( $meta_key );
		return $this->_retval;
	}

	/**
	 * Should we allow fetching meta for given content type?
	 * 
	 * @todo: implement this
	 * 
	 * @param string $content_ype
	 * @return boolean
	 */
	protected function _can_fetch_meta_for_content_type ( $content_type ) {
		return true;
	}
}