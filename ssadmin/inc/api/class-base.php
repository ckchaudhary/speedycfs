<?php 
namespace SSAdmin\API;

!defined('ABSPATH' ) ? exit() : '';

abstract class EndPoint {
	protected $_retval = [
		'status' => 'success',
		'data' => []
	];

	public function __constructor() { 

	}

	protected function _authenticate () {
		$is_token_valid = false;
		$token = isset( $_REQUEST['_token'] ) ? sanitize_text_field( $_REQUEST['_token'] ) : '';
		$consumers = maybe_unserialize( API_CONSUMERS );
		foreach ( $consumers as $consumer ) {
			if ( $token == wp_hash( $consumer ) ) {
				$is_token_valid = true;
				break;
			}
		}

		if ( !$is_token_valid ) {
			return [
				'status' => 'error',
				'data' => [
					'message' => 'Invalid security token.',
					'err_type' => 'access_denied',
				]
			];
		}

		return [ 'status' => 'success' ];
	}

	public function output () {
		$retval = $this->_authenticate();
		if ( 'error' != $retval['status'] ) {
			$retval = $this->_output();
		}

		header('Content-Type: application/json; charset=utf-8');
		die( json_encode( $retval ) );
	}

	protected abstract function _output();
}