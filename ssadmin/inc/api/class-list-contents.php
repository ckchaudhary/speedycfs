<?php 
namespace SSAdmin\API;

!defined('ABSPATH' ) ? exit() : '';

class ListContents extends EndPoint {
	
	protected $_content_ids = [];

	protected function _output() {
		$this->_query_items();

		if ( $this->_retval[ 'data' ][ 'total_items' ] > 0 && $this->_retval[ 'data' ][ 'num_items' ] > 0 ) {
			$this->_fetch_details();
		} else {
			$this->_retval[ 'data' ][ 'status' ] = 'error';
			$this->_retval[ 'data' ][ 'message' ] = 'No items found';
			$this->_retval[ 'data' ][ 'err_type' ] = 'no_items_found';
		}

		return $this->_retval;
	}

	protected function _query_items () {
		$db = db();

		$select_count = "SELECT COUNT( c.id ) ";
		$select_rows = "SELECT c.id ";

		$replacements_count = [];
		$replacements_rows = [];

		$from = " FROM {$db->table_prefix()}content c ";

		$where = [ '1=1' ];

		// status
		$where[] = "c.status = 'active'";

		// type
		$types = isset( $_GET['type'] ) && !empty( $_GET['type'] ) ? explode( ',', $_GET['type'] ) : [ 'blog' ];
		$all_types = [ 'blog', 'page', 'gallery' ];
		$sql_placeholders = [];
		foreach ( $types as $t ) {
			if ( in_array( $t, $all_types ) ) {
				$types_allowed[] = $t;
				$replacements_count[] = $t;
				$replacements_rows[] = $t;
				$sql_placeholders[] = '?';
			}
		}
		$where[] = " c.type IN ( " . implode( ',', $sql_placeholders ) . " )";

		$content_ids = [];
		if ( isset( $_GET['id'] ) && !empty( $_GET['id'] ) ) {
			$temp = sanitize_text_field( $_GET['id'] );
			$temp = explode( ',', $temp );
			foreach ( $temp as $id ) {
				$id = trim( $id );
				$id = absint( $id );
				if ( $id > 0 ) {
					$content_ids[] = $id;
				}
			}
		}

		$identifiers = [];
		if ( isset( $_GET['slug'] ) && !empty( $_GET['slug'] ) ) {
			$temp = sanitize_text_field( $_GET['slug'] );
			$temp = explode( ',', $temp );
			foreach ( $temp as $slug ) {
				$identifiers[] = trim( $slug );
			}
		}

		// Filter by id or slug/identifier
		if ( !empty( $content_ids ) || !empty( $identifiers ) ) {
			if ( !empty( $content_ids ) ) {
				$sql_placeholders = [];
				foreach ( $content_ids as $content_id ) {
					$replacements_count[] = $content_id;
					$replacements_rows[] = $content_id;
					$sql_placeholders[] = '?';
				}

				$where[] = " c.id IN ( " . implode( ',', $sql_placeholders ) . " )";
			}

			if ( !empty( $identifiers ) ) {
				$sql_placeholders = [];
				foreach ( $identifiers as $identifier ) {
					$replacements_count[] = $identifier;
					$replacements_rows[] = $identifier;
					$sql_placeholders[] = '?';
				}

				$where[] = " c.identifier IN ( " . implode( ',', $sql_placeholders ) . " )";
			}
		} else {
			// Search parameter
			$search_by = isset( $_GET['search'] ) && !empty( $_GET['search'] ) ? sanitize_text_field( $_GET['search'] ) : '';
			if ( !empty( $search_by ) ) {
				$replacements_count[] = '%' . $search_by . '%';
				$replacements_rows[] = '%' . $search_by . '%';
				$where[] = " c.title LIKE ? ";
			}
		}

		$where_str = ' WHERE ' . implode( ' AND ', $where );

		$sql_items = $select_count . $from . $where_str;
		$total_items = $db->query( $sql_items, $replacements_count )->fetchVar();
		if ( $total_items > 0 ) {
			$this->_retval[ 'data' ][ 'total_items' ] = $total_items;

			// order by clause
			$order_by = isset( $_GET[ 'order_by' ] ) && !empty( $_GET[ 'order_by' ] ) ? sanitize_text_field( $_GET[ 'order_by' ] ) : 'date';
			if ( 'date' == $order_by ) {
				$order_by = 'created_at';
			}
			$allowed_order_by = [ 'id', 'title', 'created_at' ];
			if ( !in_array( $order_by, $allowed_order_by ) ) {
				$order_by = 'created_at';
			}

			$order = isset( $_GET[ 'order_by' ] ) && !empty( $_GET[ 'order_by' ] ) ? strtoupper( $_GET[ 'order_by' ] ) : 'DESC';
			$order = 'ASC' == $order ? 'ASC' : 'DESC';

			// Limit clause
			$offset = isset( $_GET[ 'offset' ] ) && !empty( absint( $_GET[ 'offset' ] ) ) ? absint( $_GET[ 'offset' ] ) : 0;
			if ( $offset > $total_items ) {
				$offset = $total_items - 1;
			}
			$this->_retval[ 'data' ][ 'offset' ] = $offset;
			$max = isset( $_GET[ 'max' ] ) && !empty( absint( $_GET[ 'max' ] ) ) ? absint( $_GET[ 'max' ] ) : 10;

			$sql_rows = $select_rows . $from . $where_str . ' ORDER BY ' . $order_by . ' ' . $order . ' LIMIT ' . $offset . ', ' . $max;
			$result_rows = $db->query( $sql_rows, $replacements_rows )->fetchAll();
			if ( !empty( $result_rows ) ) {
				$this->_retval[ 'data' ][ 'num_items' ] = count( $result_rows );
				foreach ( $result_rows as $row ) {
					$this->_content_ids[] = $row[ 'id' ];
				}
			}
		}
	}

	protected function _fetch_details () {
		$meta_to_fetch = isset( $_GET[ 'get_meta' ] ) && !empty( $_GET[ 'get_meta' ] ) ? explode( ',', $_GET[ 'get_meta' ] ) : '';

		// Fetch details of each item
		foreach ( $this->_content_ids as $content_id ) {
			$content_obj = new \SSAdmin\ContentItem( $content_id );
			$item_details = [];
			$item_details[ 'id' ] = $content_obj->id;
			$item_details[ 'title' ] = $content_obj->title;
			$item_details[ 'type' ] = $content_obj->type;
			$item_details[ 'subtype' ] = $content_obj->subtype;
			$item_details[ 'slug' ] = $content_obj->identifier;
			$item_details[ 'details' ] = $content_obj->details;
			$item_details[ 'created_at' ] = $content_obj->created_at;

			if ( !empty( $meta_to_fetch ) ) {
				foreach ( $meta_to_fetch as $meta_key ) {
					$meta_key = sanitize_text_field( $meta_key );
					$item_details[ 'meta' ][ $meta_key ] = $content_obj->get_meta( $meta_key );
				}
			}

			$this->_retval[ 'data' ][ 'items' ][] = $item_details;
		}
	}
}