<?php 
namespace SSAdmin\API;

!defined('ABSPATH' ) ? exit() : '';

class InvalidRequest extends EndPoint {
	protected function _output () {
		return [
			'status' => 'error',
			'data' => [
				'err_type' => 'invalid_endpoint',
				'message' => 'This API request is invalid.'
			],
		];
	}
}