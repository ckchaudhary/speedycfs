<?php 
namespace SSAdmin\API;

!defined('ABSPATH' ) ? exit() : '';

class Factory {
	/**
	 * @param array $grid_atts 
	 * @return \SSAdmin\API\Endpoint
	 */
	public static function get () {
		$handler  = false;
		$endpoint = isset( $_REQUEST['endpoint'] ) ? sanitize_text_field( $_REQUEST['endpoint'] ) : '';
		
		if ( 'list-contents' == $endpoint ) {
			require_once ABSPATH . 'inc/api/class-list-contents.php';
			$handler = new ListContents();
		} else if ( 'get-meta' == $endpoint ) {
			require_once ABSPATH . 'inc/api/class-meta.php';
			$handler = new Meta();
		}

		if ( empty( $handler ) ) {
			require_once ABSPATH . 'inc/api/class-invalid.php';
			$handler = new InvalidRequest();
		}

		return $handler;
	}
}