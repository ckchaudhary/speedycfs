<?php 
!defined( '\ABSPATH' ) ? exit() : '';

function create_nonce ( $action = -1 ) {
    $i = nonce_tick();
    return substr( wp_hash( $i . '|' . $action, 'nonce' ), -12, 10 );
}

function verify_nonce ( $nonce, $action = -1 ) {
    $nonce = (string) $nonce;
    if ( empty( $nonce ) ) {
        return false;
    }
 
    $i = nonce_tick();
 
    // Nonce generated 0-12 hours ago.
    $expected = substr( wp_hash( $i . '|' . $action, 'nonce' ), -12, 10 );
    if ( hash_equals( $expected, $nonce ) ) {
        return 1;
    }
 
    // Nonce generated 12-24 hours ago.
    $expected = substr( wp_hash( ( $i - 1 ) . '|' . $action, 'nonce' ), -12, 10 );
    if ( hash_equals( $expected, $nonce ) ) {
        return 2;
    }

    // Invalid nonce.
    return false;
}

function nonce_tick () {
    $nonce_life = DAY_IN_SECONDS;
    return ceil( time() / ( $nonce_life / 2 ) );
}

function wp_hash ( $data ) {
    return hash_hmac( 'md5', $data, SSADMIN_ENCRYPTION_SECRET );
}

class Authenticator {
	public function constructor () {
	}

	public function handle_form () {
		if ( !isset( $_POST[ 'process_login' ] ) ) {
			return false;
		}

		// Check if all input is correct
		$validation = $this->_validate();
		if ( ! $validation[ 'is_valid' ] ) {
			add_notice( $validation[ 'data' ][ 'message'], [ 'type' => 'danger' ] );
			return false;
		}

		$username = sanitize_text_field( $_POST[ 'login_name' ] );
		$password = md5( $_POST['login_password'] );

		$table_prefix = db()->table_prefix();
		$user_details = db()->query( "SELECT id, level FROM {$table_prefix}account WHERE username = ? AND password = ? AND status = 0", [ $username, $password ] )->fetchArray();
		if ( !empty( $user_details ) && isset( $user_details[ 'id' ] ) && !empty( $user_details[ 'id' ] ) ) {
			session_regenerate_id();
			$_SESSION['user_id'] = $user_details[ 'id' ];
			$_SESSION['user_level'] = $user_details[ 'level' ];
			add_notice( "Login successful.", [ 'type' => 'success' ] );
			return true;
		} else {
			add_notice( 'Login details incorrect', [ 'type' => 'danger' ] );
		}

		return false;
	}

	protected function _validate () {
		// Verify nonce
		$nonce = isset( $_POST[ '_nonce' ] ) ? sanitize_text_field( $_POST[ '_nonce' ] ) : '';
		if ( ! $nonce ) {
			return [
				'is_valid' => false,
				'data' => [ 'message' => 'Invalid submission!' ],
			];
		}

		if ( ! verify_nonce( $nonce, 'login' ) ) {
			return [
				'is_valid' => false,
				'data' => [ 'message' => 'Invalid submission!' ],
			];
		}

		// Honeypot!
		$honeypot = isset( $_POST['hp_email'] ) ? $_POST['hp_email'] : '';
		if ( $honeypot ) {
			return [
				'is_valid' => false,
				'data' => [ 'message' => 'Invalid submission!' ],
			];
		}

		// Check if username and password provided
		$username = isset( $_POST['login_name'] ) ? trim( $_POST['login_name'] ) : '';
		$password = isset( $_POST['login_password'] ) ? trim( $_POST['login_password'] ) : '';
		if ( empty( $username ) || empty( $password ) ) {
			return [
				'is_valid' => false,
				'data' => [ 'message' => 'Please provide username and password first!' ],
			];
		}

		// So far, so good.
		return [ 'is_valid' => true ];
	}

	public function logout () {
		unset( $_SESSION['user_id'] );
		unset( $_SESSION['user_level'] );
		session_regenerate_id();
		return true;
	}
}

/**
 * Get the id of current logged in user.
 * 
 * @return int
 */
function get_current_user_id () {
	return isset( $_SESSION['user_id'] ) ? $_SESSION['user_id'] : false;
}

/**
 * Get the level of current logged in user.
 * 0 : user not logged in
 * 1 : Admin
 * 2 : Editor
 * 
 * @return int
 */
function get_current_user_level () {
	return $_SESSION['user_level'];
}

/**
 * Check if a given user can perform given action.
 * 
 * @todo: implement the actual logic
 * 
 * @param int $user_id 
 * @param string $capability
 * 
 * @return boolean
 */
function user_can ( $user, $capability ) {
	if ( empty( $user ) ) {
		return false;
	}

	return true;
}

/**
 * Check if a user is logged in.
 * 
 * @return boolean
 */
function is_user_logged_in () {
	return get_current_user_id() ? true : false;
}