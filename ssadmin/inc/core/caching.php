<?php 
!defined( 'ABSPATH' ) ? exit() : '';

class LogiPrismCache {
	#region Bootstrap
	private $_data = [];

	private function __construct () {

	}

	public static function instance() {
		static $self;

		if ( !isset( $self ) ) {
			$self = new self();
		}

		return $self;
	}

	#endregion

	public function get ( $key, $group = 'global' ) {
		if ( !isset( $this->_data[ $group ] ) ) {
			return null;
		}

		if ( !isset( $this->_data[ $group ][ $key ] ) ) {
			return null;
		}

		return $this->_data[ $group ][ $key ];
	}

	public function update ( $key, $value, $group = 'global' ) {
		if ( !isset( $this->_data[ $group ] ) ) {
			$this->_data[ $group ] = [];
		}

		$this->_data[ $group ][ $key ] = $value;
		return true;
	}

	public function delete ( $key, $group = 'global' ) {
		if ( !isset( $this->_data[ $group ] ) ) {
			return false;
		}

		if ( !isset( $this->_data[ $group ][ $key ] ) ) {
			return false;
		}

		unset( $this->_data[ $group ][ $key ] );
		return true;
	}

	public function delete_group ( $group ) {
		if ( !isset( $this->_data[ $group ] ) ) {
			return false;
		}

		unset( $this->_data[ $group ] );
		return true;
	}
	
}

/**
 * @return \LogiPrismCache
 */
function cache () {
	return LogiPrismCache::instance();
}