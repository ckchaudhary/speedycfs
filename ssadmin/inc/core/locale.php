<?php 
!defined( 'ABSPATH' ) ? exit() : '';

define( 'DATE_FORMAT', 'j\<\s\u\p\>S\<\/\s\u\p\> M \'y' );
define( 'TIME_FORMAT', 'H:i:s' );

// Set default date time zone
date_default_timezone_set( \TIME_ZONE );

/**
 * @return \datetimezone
 */
function wp_timezone () {
	return new datetimezone( \TIME_ZONE );
}