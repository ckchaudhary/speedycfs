<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// set the default date time zone etc
include_once ABSPATH . 'inc/core/locale.php';

// Formatting and sanitization helper functions
include_once ABSPATH . 'inc/core/formatting.php';

// database interface
include_once ABSPATH . 'inc/core/db.php';

// Simple Caching interface
include_once ABSPATH . 'inc/core/caching.php';

// authentication and stuff
include_once ABSPATH . 'inc/core/authentication.php';