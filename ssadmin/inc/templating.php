<?php 
!defined( 'ABSPATH' ) ? exit() : '';


// root directory
define( 'TEMPLATE_PATH', ABSPATH . 'themes/sb-admin/' );

// The singleton object that has details of current http request
include_once ABSPATH . 'inc/templating/class-request.php';
// body_class, site_title, header footer etc.
include_once ABSPATH . 'inc/templating/template-tags.php';

// show error, show success etc.
include_once ABSPATH . 'inc/templating/misc.php';

function current_request() {
	return \LogiPrism\SS\Request::instance();
}

current_request()->parse();