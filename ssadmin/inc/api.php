<?php 
!defined( 'ABSPATH' ) ? exit() : '';

/**
 * This file is included just in time by templates/ajax/api.php
 */

require_once ABSPATH . 'inc/api/class-factory.php';
require_once ABSPATH . 'inc/api/class-base.php';