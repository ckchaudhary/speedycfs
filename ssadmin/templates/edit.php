<?php 
/**
 * List of 'pages'
 */
!defined( 'ABSPATH' ) ? exit() : '';

$error = [];

$content_id = isset( $_GET['c'] ) ? absint( $_GET['c'] ) : 0;
if ( empty( $content_id ) ) {
	$error = [
		'type' => 'empty_id',
		'message' => 'This request is invalid.'
	];
} else {
	$content_obj = new \SSAdmin\ContentItem( $content_id );
	if ( !$content_obj->id ) {
		$error = [
			'type' 		=> 'invalid_id',
			'message' 	=> 'This item does not exist.'
		];	
	}
}

if ( !empty( $error ) ) {
	add_notice( $error['message'], [ 'is_dismissable' => false, 'type' => 'danger' ] );
} else {
	$content_obj->maybe_update();

	current_request()->set_details( [
		'title' 		=> 'Edit ' . $content_obj->type,
		'body_classes' 	=> [ 'edit', 'type-' . $content_obj->type ],
	] );
}

include TEMPLATE_PATH . 'parts/header.php';

if ( empty( $error ) ) {
	?>
	<h1>Edit - <?php echo $content_obj->type;?></h1>
	<div class='wrap mt-5'>
		<?php get_template_part( 'edit/type', $content_obj->type, [ 'content_obj' => $content_obj ] );?>
	</div>
	<?php 
}

include TEMPLATE_PATH . 'parts/footer.php';