<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

$valid_ctypes = [];
$valid_ctypes[] = CT_BLOG;// blogs
$valid_ctypes[] = 'gallery';

$ctype = isset( $_GET['ctype'] ) ? $_GET['ctype'] : '';
$is_type_valid = false;
if ( !empty( $ctype ) && in_array( $ctype, $valid_ctypes ) ) {
	$is_type_valid = true;
}

if ( !$is_type_valid ) {
	add_notice( 'Invalid content type!', [ 'type' => 'danger', 'is_dismissable' => false ] );
} else {
	if ( isset( $_POST[ 'add_new_item' ] ) ) {
		$error = '';

		$nonce = isset( $_POST['_nonce'] ) ? $_POST['_nonce'] : '';
		if ( empty( $nonce ) || !verify_nonce( $nonce, 'add-item' ) ) {
			$error = "Invalid security token";
		}

		if ( !$error ) {
			$title = isset( $_POST[ 'title' ] ) ? sanitize_text_field( $_POST[ 'title' ] ) : '';
			if ( empty( $title ) ) {
				$error = 'Please enter the title first.';
			}
		}

		if ( !$error ) {
			$content_obj = new \SSAdmin\ContentItem();
			$content_obj->type = $ctype;
			$content_obj->title = $title;
			$new_content_id = $content_obj->create();
			if ( $new_content_id ) {
				redirect( HOME_URL . 'edit/?c=' . $new_content_id );
			}
		}

		if ( $error ) {
			add_notice( $error, [ 'type' => 'danger' ] );
		}
	}	
}

current_request()->set_details( [
	'title' 		=> 'Add new ' . $ctype,
	'body_classes' 	=> [ 'add', $ctype ],
] );

include TEMPLATE_PATH . 'parts/header.php';

if ( $is_type_valid ) :
?>
<h1>Add new <?php echo ucfirst( $ctype ); ?></h1>
<form method='POST'>
	<input type='hidden' name='_nonce' value='<?php echo create_nonce( 'add-item' );?>'>
	<input type='hidden' name='add_new_item' value='yes'>

	<div class="mb-3">
    	<label for="title" class="form-label">Title</label>
    	<input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp">
    	<div id="titleHelp" class="form-text">Enter a suitable title for the new item. You can always change it later.</div>
  	</div>

	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php 
endif;

include TEMPLATE_PATH . 'parts/footer.php';