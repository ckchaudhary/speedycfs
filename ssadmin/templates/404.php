<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> '404 Not found',
	'body_classes' 	=> [ '404' ],
] );

include TEMPLATE_PATH . 'parts/header.php';
?>
<h1 class='display-1'>Not found</h1>
<p>This links is broken!</p>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';