<?php 
/**
 * List of 'pages'
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Manage Galleries - SSAdmin',
	'body_classes' 	=> [ 'gallery' ],
] );

include TEMPLATE_PATH . 'parts/header.php';

$media_list = new \SSAdmin\GalleryListTable();
?>
<h1>Galleries</h1>
<div class='wrap mt-5'>
	<?php $media_list->output();?>
</div>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';