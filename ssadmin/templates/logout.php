<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Login - SSAdmin',
	'body_classes' 	=> [ 'login' ],
] );

$authenticator = new Authenticator();
$authenticator->logout();
add_notice( 'You have been logged out.', [ 'type' => 'success' ] );

include TEMPLATE_PATH . 'parts/header.php';
?>
<p>Redirecting you in 3 seconds...</p>
<script>
(function () {
	'use strict'
	
	setTimeout( function(){ window.location.href = '<?php echo HOME_URL;?>'; }, 3000 );
})()
</script>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';