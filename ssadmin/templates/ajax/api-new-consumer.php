<?php 
!defined( 'ABSPATH' ) ? exit() : '';

/**
 * Print out an authentication token for given api consumer(id)
 * This authentication token must be used to send all api requests from that consumer.
 * Further more, that consumer must be added to the list of whitelisted domains for api calls in config.php
 * Otherwise API calls will be discarded.
*/
$id = isset( $_GET['id'] ) ? trim( $_GET['id'] ) : '';
echo $id ? wp_hash( $id ) : '';