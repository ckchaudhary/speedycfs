<?php 
!defined( 'ABSPATH' ) ? exit() : '';

// File to process the ajax request sent to upload files.
$retval = [
	'status' => 'success',
	'data' => [],
];

if ( ! user_can( get_current_user_id(), 'upload-media' ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Your account does not have necessary privileges to upload files.';
	die( json_encode( $retval ) );
}

// verify nonce
$nonce = isset($_POST['_nonce']) ? $_POST['_nonce'] : '';
if ( empty( $nonce ) || !verify_nonce( $nonce, 'upload_media' ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Invalid security token. Please refresh the page and try again.';
	die( json_encode( $retval ) );
}

if ( !isset( $_FILES[ 'chosen_file' ] ) || empty( $_FILES[ 'chosen_file' ] ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Please choose a file first.';
	die( json_encode( $retval ) );
}

// $allowed_extensions = [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'zip' ];
$allowed_extensions = [ 'png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp' ];
$max_size = 50 * 1024 * 1024;// 50mb

$fileName 		= $_FILES['chosen_file']['name'];
$fileSize 		= $_FILES['chosen_file']['size'];
$fileTmpName  	= $_FILES['chosen_file']['tmp_name'];
$fileType 		= $_FILES['chosen_file']['type'];
$name_parts 	= explode( '.', $fileName );
$fileExtension 	= strtolower( end( $name_parts ) );

if ( !in_array( $fileExtension, $allowed_extensions ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Only .' . implode( ', .', $allowed_extensions ) . ' files are allowed.';
	die( json_encode( $retval ) );
}

if ( $fileSize > $max_size ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Files larger than 50mb can\'t be uploaded.';
	die( json_encode( $retval ) );
}

$new_name = md5( $fileName ) . '_' . str_replace( '.', '-', microtime( true ) ) . '.' . $fileExtension;

$upload_base_dir = dirname( ABSPATH, 1 ) . DIRECTORY_SEPARATOR . UPLOADS_DIR;
$current_upload_dir =  date( 'Y' ) . DIRECTORY_SEPARATOR . date( 'm' );

if ( !file_exists( $upload_base_dir . DIRECTORY_SEPARATOR . $current_upload_dir ) ) {
	mkdir( $upload_base_dir . DIRECTORY_SEPARATOR . $current_upload_dir, 0777, true );
}

$uploadPath = $upload_base_dir . DIRECTORY_SEPARATOR . $current_upload_dir . DIRECTORY_SEPARATOR . $new_name;

$didUpload = move_uploaded_file( $fileTmpName, $uploadPath );
if ( $didUpload ) {
	$upload_relative_url = $current_upload_dir . '/' . $new_name;
	$upload_relative_url = str_replace( DIRECTORY_SEPARATOR, '/', $upload_relative_url );

	$content_obj = new \SSAdmin\ContentItem();
	$content_obj->type = 'media';
	$content_obj->title = $fileName;
	$new_content_id = $content_obj->create();
	if ( $new_content_id ) {
		// All good
		$content_obj->status = 'active';
		$content_obj->update();

		$content_obj->update_meta( 'relative_url', $upload_relative_url );
		$content_obj->update_meta( 'relative_path', $current_upload_dir . DIRECTORY_SEPARATOR . $new_name );
		$data = [
			'preview'	=> get_media_preview( $content_obj->id ),
			'title' 	=> $content_obj->title,
			'date' 		=> date( DATE_FORMAT, $content_obj->created_at ) . '&nbsp;<small>' . date( TIME_FORMAT, $content_obj->created_at )  . '</small>',
			'action'	=> 
				// View button
				sprintf( "<a href='%s' target='_blank' class='btn btn-secondary'>View <sup><i class='bi bi-box-arrow-in-up-right'></i></sup></a>&nbsp;", SITE_URL .  UPLOADS_DIR . '/' . $upload_relative_url ) .
				// Copy url
				sprintf( "<a href='#' class='btn btn-secondary' data-copy_text data-text_to_copy='%s'>Copy url</a>&nbsp;", SITE_URL .  UPLOADS_DIR . '/' . $upload_relative_url ) .
				// Delete
				sprintf( "<a href='%s' class='btn btn-danger btn_delete_item'>Delete</a>&nbsp;", esc_attr( get_delete_url( $content_obj->id ) ) )
		];

		$retval[ 'data' ] = $data;
	}
}

die( json_encode( $retval ) );