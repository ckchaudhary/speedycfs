<?php 
!defined( 'ABSPATH' ) ? exit() : '';

/*
 * Process the ajax request sent to delete any content.
*/

$retval = [
	'status' => 'success',
	'data' => [],
];

// verify nonce
$nonce = isset( $_GET['_nonce'] ) ? $_GET['_nonce'] : '';
if ( empty( $nonce ) || !verify_nonce( $nonce, 'delete_content' ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Invalid security token. Please refresh the page and try again.';
	die( json_encode( $retval ) );
}

if ( ! user_can( get_current_user_id(), 'delete_content' ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Your account does not have necessary privileges to perform this action.';
	die( json_encode( $retval ) );
}

if ( !isset( $_GET[ 'c' ] ) || empty( absint( $_GET[ 'c' ] ) ) ) {
	$retval[ 'status' ] = 'error';
	$retval[ 'data' ][ 'message' ] = 'Invalid Request.';
	die( json_encode( $retval ) );
}

$content_obj = new \SSAdmin\ContentItem( absint( $_GET[ 'c' ] ) );
$retval = $content_obj->delete();
if ( 'success' == $retval[ 'status' ] ) {
	$retval['data'] = [
		'message' => 'Deleted successfuly.',
		'list_url' => HOME_URL . $content_obj->type,
	];
}

die( json_encode( $retval ) );