<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Login - SSAdmin',
	'body_classes' 	=> [ 'login' ],
] );

$authenticator = new Authenticator();
$authenticator->handle_form();

include TEMPLATE_PATH . 'parts/header.php';
if ( !is_user_logged_in() ) :
?>
<h1 class='mb-3 display-6'>Login to continue..</h1>
<form method='post' action='<?php echo HOME_URL;?>' class="g-3 needs-validation" novalidate>
  	<input type='hidden' name='_nonce' value="<?php echo create_nonce( 'login' );?>" >
	<input type='hidden' name='process_login' value='1' >

	<div class="input-group has-validation mb-3">
  		<span class="input-group-text" id="username-addon">Username</span>
  		<input type="text" name="login_name" value="<?php echo isset( $_POST['login_name'] ) ? esc_attr( $_POST['login_name'] ) : '';?>" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="username-addon" required >
		<div class="invalid-feedback">Please enter your username.</div>
	</div>

	<div class="input-group has-validation mb-3">
  		<span class="input-group-text" id="password-addon">Password</span>
  		<input type="password" name="login_password" class="form-control" placeholder="Password" aria-label="Username" aria-describedby="password-addon" required>
		<div class="invalid-feedback">Please enter your password.</div>
	</div>

	<input type="text" name="hp_email" value="" style="display:none !important" tabindex="-1" autocomplete="off">

  	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<script>
(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
</script>
<?php else : ?>
	<?php 
	if ( isset( $_POST['process_login'] ) ) {
		// user was just logged in. Success message was already shown.
	} else {
		add_notice( 'You are logged in already.', [ 'is_dismissable' => false ] );
	}
	?>
	<p>Redirecting you in 3 seconds...</p>
	<script>
	(function () {
		'use strict'
		
		setTimeout( function(){ window.location.href = '<?php echo HOME_URL;?>'; }, 3000 );
	})()
	</script>
<?php endif;
include TEMPLATE_PATH . 'parts/footer.php';