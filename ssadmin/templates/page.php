<?php 
/**
 * List of 'pages'
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Manage Pages - SSAdmin',
	'body_classes' 	=> [ 'pages', 'list-table' ],
] );

include TEMPLATE_PATH . 'parts/header.php';

$pages_list = new \SSAdmin\PageListTable();
?>
<h1>Pages</h1>
<p>Manage pages.</p>
<div class='wrap mt-5'>
	<?php $pages_list->output();?>
</div>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';