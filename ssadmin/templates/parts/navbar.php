<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  	<div class="container-fluid">
    	<a class="navbar-brand" href="<?php echo HOME_URL;?>"><i class="bi bi-house-fill"></i></a>
    	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      		<span class="navbar-toggler-icon"></span>
    	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<?php 
				$current_page = 'home';
				$page_names = current_request()->get_path_parts();
				if ( !empty( $page_names ) ) {
					$current_page = $page_names[0];
				}
				?>
				<li class="nav-item">
					<a class="nav-link <?php echo $current_page == 'home' ? 'active' : '';?>" aria-current="page" href="<?php echo HOME_URL;?>">Home</a>
				</li>

				<?php if ( is_user_logged_in() ) : ?>

					<li class="nav-item">
						<a class="nav-link <?php echo $current_page == 'page' ? 'active' : '';?>" href="<?php echo HOME_URL;?>page"><i class="bi bi-files"></i>&nbsp;Pages</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?php echo $current_page == 'blog' ? 'active' : '';?>" href="<?php echo HOME_URL;?>blog"><i class="bi bi-pencil-square"></i>&nbsp;Blogs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?php echo $current_page == 'media' ? 'active' : '';?>" href="<?php echo HOME_URL;?>media"><i class="bi bi-files"></i>&nbsp;Media</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?php echo $current_page == 'gallery' ? 'active' : '';?>" href="<?php echo HOME_URL;?>gallery"><i class="bi bi-images"></i>&nbsp;Gallery</a>
					</li>
					
					<lic class="nav-item">
						<a class="nav-link <?php echo $current_page == 'logout' ? 'active' : '';?>" href="<?php echo HOME_URL;?>logout"><i class="bi bi-box-arrow-right"></i>&nbsp;Logout</a>
					</lic>

				<?php endif; ?>
			</ul>
    	</div>
  	</div>
</nav>