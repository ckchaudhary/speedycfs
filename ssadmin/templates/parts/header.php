<!doctype html>
<html lang="en">
  	<head>
    	<?php header_metas(); ?>
		<?php header_assets(); ?>
  	</head>

  	<body class="minimal">

	  	<div class="page-gradient-bg"></div>
		<div class="page-gradient-bg bg2"></div>
		<div class="page-gradient-bg bg3"></div>

		<div class="container">

		<?php include TEMPLATE_PATH . 'parts/navbar.php'; ?>

		<div class="p-3 border" style="background-color: #fff">
			<?php _print_notices();?>