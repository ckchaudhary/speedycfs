<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Home Page',
	'body_classes' 	=> [ 'home' ],
] );

include TEMPLATE_PATH . 'parts/header.php';
?>
<h1 class='display-1'><i class="bi bi-body-text"></i></h1>
<p>Go to individual menu items for corresponding functionality.</p>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';