<?php 
!defined( 'ABSPATH' ) ? exit() : '';

include_once ABSPATH . 'inc/api.php';
$api_handler = SSAdmin\API\Factory::get();
$api_handler->output();