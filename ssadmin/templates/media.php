<?php 
/**
 * List of 'pages'
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Manage Uploads - SSAdmin',
	'body_classes' 	=> [ 'media' ],
] );

include TEMPLATE_PATH . 'parts/header.php';

$media_list = new \SSAdmin\MediaListTable();
?>
<h1>Uploads <a class="btn btn-primary btn-sm" id="btn_add_new_media" href="#">Add New</a></h1>
<p>Upload or remove images and other media files here. You can copy the url of uploaded file and use it in blog posts, page contents, sliders etc.</p>
<div class='wrap mt-5'>
	<?php $media_list->output();?>
</div>

<div style="display: none;">
	<form method="post" action="<?php echo HOME_URL;?>ajax/upload-media" enctype="multipart/form-data" id="frm_hidden_uploader">
		<input type="hidden" name="_nonce" value="<?php echo create_nonce( 'upload_media' );?>">

		<input type="file" name="chosen_file">
	</form>
</div>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';