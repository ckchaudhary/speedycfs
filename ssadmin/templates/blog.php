<?php 
/**
 * List of 'pages'
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> 'Manage Blog Posts - SSAdmin',
	'body_classes' 	=> [ 'blogs', 'list-table' ],
] );

include TEMPLATE_PATH . 'parts/header.php';

$pages_list = new \SSAdmin\PostListTable();
?>
<h1>Blog Posts <a class="btn btn-primary btn-sm" href='<?php echo HOME_URL . 'add/?ctype=' . CT_BLOG;?>'>Add New</a></h1>
<p>Manage blog posts.</p>
<div class='wrap mt-5'>
	<?php $pages_list->output();?>
</div>
<?php 
include TEMPLATE_PATH . 'parts/footer.php';