<?php 
// template for editing blog posts
$content_obj = $args[ 'content_obj' ];
?>
<form method='POST' action='<?php echo HOME_URL . 'edit/?c=' . $content_obj->id;?>'>
	<input type='hidden' name='_nonce' value="<?php echo create_nonce( 'edit-content' );?>" >
	<input type='hidden' name='content_id' value='<?php echo $content_obj->id;?>' >
	<input type='hidden' name='update_content' value='yes' >

	<div class="row">
    	<div class="col-md-8">
			<?php // Gallery Items ?>
			<div class='editor-widget field-meta field-gallery-items'>
				<div class="p-3 mb-2 bg-light text-dark clearfix">
					<h3>Gallery Items <a class="btn btn-primary btn-sm" id="btn-add-gallery-item" href='#'>Add New</a></h3>

					<input type="hidden" name="meta[gallery_items]" >
					<div class="gallery-items-list">

					</div>
				</div>
			</div>
		</div>

    	<div class="col-md-4">
			<?php // title ?>
			<div class='editor-widget field-title'>
				<div class="p-3 mb-2 bg-light text-dark clearfix">
					<label for="title" class="form-label">Title</label>
  					<input type="text" class="form-control" id="title" name="title" placeholder="Enter title here" value='<?php echo esc_attr( $content_obj->form_data( 'title' ) );?>' >
				</div>
			</div>

			<?php // Details ?>
			<div class='editor-widget field-details'>
				<div class="p-3 mb-2 bg-light text-dark">
					<label class="form-label" for="details">Description</label>
					<textarea class="form-control" name="details" id="details"><?php echo esc_textarea( $content_obj->form_data( 'details' ) );?></textarea>
				</div>
			</div>

			<?php // Status - can't change ?>
			<input type="hidden" name="status" value="active">

			<?php // Update ?>
			<div class='editor-widget field-update'>
				<div class="p-3 mb-2 bg-light text-dark">
					<input type="submit" value="Update" class="btn btn-primary btn-lg"> &nbsp;

					<?php if ( 'page' !== $content_obj->type ) : ?>
						<a class="btn btn-outline-danger btn-sm" id='btn_delete_content' href="<?php echo esc_attr( get_delete_url( $content_obj->id ) );?>">Delete</a>
					<?php endif; ?>
				</div>
			</div>

		</div>
  	</div>
</form>

<script>
<?php 
$items = $content_obj->get_meta( 'gallery_items' );
if ( empty( $items ) ) {
	$items = "[]";
}
?>
window.gallery_manager_items = <?php echo $items;?>;
</script>