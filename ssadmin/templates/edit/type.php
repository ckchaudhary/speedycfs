<?php 
// template for editing blog posts
$content_obj = $args[ 'content_obj' ];
?>
<form method='POST' action='<?php echo HOME_URL . 'edit/?c=' . $content_obj->id;?>'>
	<input type='hidden' name='_nonce' value="<?php echo create_nonce( 'edit-content' );?>" >
	<input type='hidden' name='content_id' value='<?php echo $content_obj->id;?>' >
	<input type='hidden' name='update_content' value='yes' >

	<div class="row">
    	<div class="col-md-8">
			
			<?php // title ?>
			<div class='editor-widget field-title'>
				<div class="p-3 mb-2 bg-light text-dark clearfix">
					<label for="title" class="form-label">Title</label>
  					<input type="text" class="form-control" id="title" name="title" placeholder="Enter title here" value='<?php echo esc_attr( $content_obj->form_data( 'title' ) );?>' >
					
					<div class="float-end">
						<input type="hidden" name="content_identifier" value="<?php echo esc_attr( $content_obj->form_data( 'identifier' ) );?>" >
						<div class="input-group mb-3">
							<span class="input-group-text text-secondary"><i class="bi bi-link"></i>&nbsp;<span class='content_url'><?php echo SITE_URL . $content_obj->type . '/' . $content_obj->identifier;?></span></span>
							<button id='btn_reset_url' type="button" class="form-control btn btn-outline-danger btn-sm" title="Reset and autogenerate url">Reset</button>
						</div>
					</div>
				</div>
			</div>

			<?php // Details ?>
			<div class='editor-widget field-details'>
				<div class="p-3 mb-2 bg-light text-dark">
					<label class="form-label" for="details">Details</label>
					<textarea class="form-control rteditor" name="details" id="details"><?php echo esc_textarea( $content_obj->form_data( 'details' ) );?></textarea>
				</div>
			</div>
		</div>

    	<div class="col-md-4">
			<?php // Status ?>
			<div class='editor-widget field-status'>
				<?php 
				$bg_color_class = 'active' == $content_obj->status ? 'bg-success' : 'bg-warning';
				?>
				<div class="p-3 mb-2 <?php echo $bg_color_class;?> text-dark bg-opacity-25">
					<div class="input-group">
						<span class="input-group-text">Status</span>
						<select name='status' class="form-control">
							<?php 
							$all_status = [
								'active' => 'Active',
								'draft' => 'Draft',
							];

							foreach ( $all_status as $status => $label ) {
								$selected = $status == $content_obj->form_data( 'status' ) ? 'selected' : '';
								printf( "<option value='%s' %s>%s</option>", $status, $selected, $label );
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<?php // Update ?>
			<div class='editor-widget field-update'>
				<div class="p-3 mb-2 bg-light text-dark">
					<input type="submit" value="Update" class="btn btn-primary btn-lg"> &nbsp;

					<?php if ( 'page' !== $content_obj->type ) : ?>
						<a class="btn btn-outline-danger btn-sm" id='btn_delete_content' href="<?php echo esc_attr( get_delete_url( $content_obj->id ) );?>">Delete</a>
					<?php endif; ?>
				</div>
			</div>

		</div>
  	</div>
</form>