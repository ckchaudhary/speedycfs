<?php 
/**
 * The main entry point for public facing site
 */

// Load everything!
include 'init.php';

$loaded = false;
$templates_to_load = current_request()->templates_to_load();
if ( !empty( $templates_to_load ) ) {
	foreach ( $templates_to_load as $template ) {
		if ( get_template_part( $template ) ) {
			$loaded = true;
			break;
		}
	}
}
if ( !$loaded ) {
	get_template_part( '404' );
}
