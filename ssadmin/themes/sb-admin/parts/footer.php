					<?php _print_notices();?>
					</div><!-- .container-fluid px-4 -->
				</main>

				<footer class="py-4 bg-light mt-auto">
					<div class="container-fluid px-4">
						<div class="d-flex align-items-center justify-content-between small">
							<div class="text-muted">Copyright &copy; SpeedyCFS 2022</div>
							<?php /*
							<div>
								<a href="#">Privacy Policy</a>
								&middot;
								<a href="#">Terms &amp; Conditions</a>
							</div>*/ ?>
						</div>
					</div>
				</footer>
			</div><!-- #layoutSidenav_content -->

		</div><!-- #layoutSidenav -->

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo HOME_URL?>themes/sb-admin/js/scripts.js"></script>


<script>
var SSADMIN = {
	HOME_URL : '<?php echo HOME_URL;?>',
	SITE_URL : '<?php echo SITE_URL;?>',
};
</script>
<script src="<?php echo HOME_URL;?>assets/js/utils.js"></script>
<script src="<?php echo HOME_URL;?>assets/js/common.js"></script>
<?php 
$page_names = current_request()->get_path_parts();
if ( !empty( $page_names ) && 'edit' == $page_names[0] ) {
	// <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
	?>
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
	<script src="<?php echo HOME_URL;?>assets/js/editor.js"></script>
	<?php 
}

if ( !empty( $page_names ) && 'media' == $page_names[0] ) {
	?>
	<script src="<?php echo HOME_URL;?>assets/js/jquery.form.min.js"></script>
	<script src="<?php echo HOME_URL;?>assets/js/file-uploader.js"></script>
	<?php 
}
?>

	</body>
</html>