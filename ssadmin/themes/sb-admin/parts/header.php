<!doctype html>
<html lang="en">
  	<head>
    	<?php header_metas(); ?>

		<link href="<?php echo HOME_URL;?>themes/sb-admin/css/styles.css" rel="stylesheet" />
		<link href="<?php echo HOME_URL;?>themes/sb-admin/css/override.css" rel="stylesheet" />

		<!-- Bootstrap Icons -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
		<link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/main.css">
		<?php 
		$page_names = current_request()->get_path_parts();
		if ( !empty( $page_names ) && 'edit' == $page_names[0] ) {
			?><link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet"><?php 
		}
		?>
  	</head>

  	<body class="sb-nav-fixed <?php body_class();?>">

	  	<?php get_template_part( 'parts/top-navbar' ); ?>

		<div id="layoutSidenav">
			<div id="layoutSidenav_nav">
				<?php get_template_part( 'parts/side-nav' ); ?>
			</div><!-- #layoutSidenav_nav -->

			<div id="layoutSidenav_content">
				<main>
					<div class="container-fluid p-4">
						<?php _print_notices();?>