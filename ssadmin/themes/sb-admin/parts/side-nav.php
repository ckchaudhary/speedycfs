<div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav mt-3">
                            <?php 
							$current_page = 'home';
							$page_names = current_request()->get_path_parts();
							if ( !empty( $page_names ) ) {
								$current_page = $page_names[0];
							}
							?>

							<?php if ( is_user_logged_in() ) : ?>
								<a class="nav-link <?php echo $current_page == 'page' ? 'active' : '';?>" href="<?php echo HOME_URL;?>page"><i class="bi bi-diagram-3-fill"></i>&nbsp;&nbsp;Pages</a>
								
								<a class="nav-link <?php echo $current_page == 'blog' ? 'active' : '';?>" href="<?php echo HOME_URL;?>blog"><i class="bi bi-pencil-square"></i>&nbsp;&nbsp;Blogs</a>
								
								<a class="nav-link <?php echo $current_page == 'media' ? 'active' : '';?>" href="<?php echo HOME_URL;?>media"><i class="bi bi-upload"></i>&nbsp;&nbsp;Uploads</a>
								<a class="nav-link <?php echo $current_page == 'gallery' ? 'active' : '';?>" href="<?php echo HOME_URL;?>gallery"><i class="bi bi-images"></i>&nbsp;&nbsp;Gallery</a>
								
							<?php else : ?>
								<a class="nav-link active" href="<?php echo HOME_URL;?>"><i class="bi bi-box-arrow-in-left"></i>&nbsp;Login</a>
							<?php endif; ?>

                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <?php if ( is_user_logged_in() ) : ?>
							<a class="nav-link" href="<?php echo HOME_URL;?>logout"><i class="bi bi-box-arrow-right"></i>&nbsp;Logout</a>
						<?php else : ?>
							<p>&nbsp;</p>
						<?php endif; ?>
                    </div>
                </nav>
            </div>