<?php 
/**
 * Template for home page.
 */
!defined( 'ABSPATH' ) ? exit() : '';

current_request()->set_details( [
	'title' 		=> '404 Not found',
	'body_classes' 	=> [ '404' ],
] );

get_template_part( 'parts/header' );
?>
<div class="mx-auto mt-5" style="max-width: 400px">
	<img src='<?php echo HOME_URL;?>themes/sb-admin/assets/img/error-404-monochrome.svg' style='max-width: 400px;' class='align-center'>
</div>
<?php 
get_template_part( 'parts/footer' );